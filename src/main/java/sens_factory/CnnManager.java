/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sens_factory;

import com.google.gson.Gson;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Delivery;
import contenitori.CfgConnessione;
import contenitori.Messaggio;
import contenitori.CfgSensore;
import optSuFile.OperazioniSuFile;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import partenza.Costanti;
import sensore.CnnBase;
import sensore.ConnessioneSensore;

/**
 *
 * @author universita
 */
public class CnnManager {

    private Logger logger;
    private FileHandler fh;

    private Connection cnn = null;
    private CfgConnessione cfgOpt = null;
    private Channel ch = null;

	// nomi delle code in cui sono registrato
    private String codaComandi;
    
    private Gson gson = new Gson();

    public CnnManager(){
        gson = new Gson();
        impostaFileHandler( this.getClass().getName() );

        cfgOpt = OperazioniSuFile.leggiFileConnessione();
    }
    private boolean creaConnessione(){
        CnnBase cBase = new CnnBase( cfgOpt );
        cnn = cBase.getConnessione();
        return ( null != cnn );
    }
    /**
     * Si connette a Rabbit con i dati precedentemente impostati
     */
    private void creaCanale(){

        try {
            // connetto con tutti i parametri di configurazione
            // se tutto e andato bene, posso aprire il canale di comunicazione
            ch = cnn.createChannel();
       
            initRiceviCmd();
                        
            // tutti i messaggi che arrivano a questa coda, vengono gestiti
            ch.basicConsume( codaComandi , true, cbRicevoCmd , (cancelCb)-> {} );
            
        } catch (IOException ex) {
            Logger.getLogger( CnnManager.class.getName() ).log(Level.SEVERE, null, ex);
        } 
    }
    /**
     * Si connette a Rabbit con i dati precedentemente impostati
     */
    public void attiva(){
        if( creaConnessione() ){
            creaCanale();
        } else {
            logger.severe( this.getClass().getName() + " : chiuso per problemi di connessione" );
        }
    }

    /** chiamarlo per scrivere su file le impostazioni
     * e chiude il canale
     */
    public void disattiva() {

        try {
            ch.abort();
            cnn.abort();

        } catch (IOException ex) {

            logger.log(Level.SEVERE, null, ex);
            logger.log(Level.SEVERE , " Errore chiusura canale" );

        }
    }
    
    
    
    
    
    
    private void initRiceviCmd() throws IOException {
        // idempotente
        // definisco il tipo di exchange = topic
        ch.exchangeDeclare(Costanti.DEF_EX_CMD , BuiltinExchangeType.TOPIC );
        
        // definsco una coda
        // ma la lascio fecifere al server 
        // la cida dichuarata in queso moda e:
        // non persistente      = se cade il server perdo tutto
        // esclusiva            = valida solo per la sessione in corso
        // si cancella da sola  = quando non piu in uso
        codaComandi = ch.queueDeclare().getQueue();

        //TODO: definire una diversa routing key (adesso e' citta ) perche
        // se ho sensori con nomi diversi, potrei avere problemi

        // associo la coda allo exchange
        // e dichiaro a quali topic ( pattern di topic) sono interessato
        ch.queueBind( codaComandi, Costanti.DEF_EX_CMD, Costanti.DEF_CITTA );
        
    }
    
    
    
    // cosa devo fare quando arriva un messaggio ad una certa coda
    private DeliverCallback cbRicevoCmd = new DeliverCallback() {
        
        @Override
        public void handle(String consumaTag, Delivery ricevuto) throws IOException {
        	
        	String msgRicevuto = new String( ricevuto.getBody() );
        	Messaggio cmd = gson.fromJson( msgRicevuto, Messaggio.class );
        	
        	if( cmd.nSensore.equalsIgnoreCase( Messaggio.CMD_NSENSORE_MANAGER )
			) {
        		
	        	switch ( cmd.cmd.toLowerCase() ) {
					case Messaggio.CMD_NEW_SENSORE:
						// controllo il numero maggiore
						//		magari appena avvio la classe
						
						// creo il file di configurazione e piazzo il sensore in
						//		una posizione predefinita
						
						// avvio il nuovo sensore
						// 		appena si avvia lo comunica a chiunque
						creaNuovoSensore();
						break;

                    case Messaggio.CMD_LOG_ON:
                        attivaLog();
                        break;

                    case Messaggio.CMD_LOG_OFF:
                        disattivaLog();
                        break;

                    case Messaggio.CMD_QUIT:
                        disattiva();
                        break;

					default:
						break;
				}
	        	
	        	// TODO : spostare le parti comuni qui ?! 
	        	// magari spedire adesso ?
	        	Logger.getLogger(CnnManager.class.getName())
	        	.log(Level.INFO , 
        			"[ ricevo ] da " + ricevuto.getEnvelope().getRoutingKey() + 
        			" : " + msgRicevuto 
    			);
	        	
        	}
    	}
        
    };

    /**
     * creo e faccio partire un nuovo sensore ... rimane legato a questo thread
     */
	private void creaNuovoSensore() {
		CfgSensore cfg = OperazioniSuFile.creaCfgNuovoSensore();

		if( cfg != null ){
		    new ConnessioneSensore( cfg.nSensore ).attiva();
        }

	}

    /**
     * per il log su file
     * @param nomeFile_e_Logger
     */
    private void impostaFileHandler( String nomeFile_e_Logger ) {
        if( fh == null ) {

            try {
                fh = new FileHandler("logs/" + nomeFile_e_Logger , false);
                fh.setFormatter( new SimpleFormatter() );

                logger = Logger.getLogger( nomeFile_e_Logger );
                logger.addHandler( fh );

            } catch (SecurityException | IOException e) { e.printStackTrace();}
        }
    }

    public void disattivaLog() 	{ logger.setLevel(Level.OFF); }
    public void attivaLog() 	{ logger.setLevel(Level.ALL); }
    
}
