package funzione_db;

import partenza.Costanti;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AttivaFunzione implements Runnable {

    /** coda condivisa con le funzioni di calcolo per il db 
     * accetta stringhr che poi diverranno (usare put per inserire) */
    public static final LinkedBlockingQueue< Long > coda = new LinkedBlockingQueue<>( Costanti.CODA_DIM_ID );
	/** accumulo l'ltimo tempo da elaborare */
    private long idBase;
    
    /** condizione di sopravvivenza */
    private boolean continua = true;
    
    /** Thread che inizia l'elaborazione */
    private Thread t;

    
    
	/** inizio a elaborare all'infinito */
	public AttivaFunzione( long idIniziale) { 
		this.idBase = idIniziale;
	}
    
    
    
	@Override
	public void run(  ) {
		long nuovoId = 0L;
		
		// ciclo di vita
		while( continua ) {
			try {
				
				// recupro il valore
				nuovoId = coda.take();
				
				//nuovoId = coda.poll(1, TimeUnit.SECONDS );
				
				// se quello nuovo e' un nuovo valore
				// altrimenti, ignoro e scarto
				if( nuovoId > idBase ) {
					
					// aggiorno il valore
					idBase = nuovoId;
					System.out.println( nuovoId );
					
					
					// creo e spedisco il thread
					t = new Thread( new CreaPuntoEvento( nuovoId ) );
					t.setDaemon( false );
					t.start();
					
				}
				
			} catch (InterruptedException e) {
				quit();
				Logger.getLogger(AttivaFunzione.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		
		Logger.getLogger(AttivaFunzione.class.getName()).log( Level.INFO, null, "chiuso attiva funzione" );
		
	}
	

	
	/** fermo il thread con dolcezza */ 
	public void quit() {
		
		// fermo il ciclo
		continua = false;
		
		// aggiungo un valore in caso sia bloccata
		try { coda.put( 0L ); } 
		catch (InterruptedException e) { e.printStackTrace(); }
	}
	

}
