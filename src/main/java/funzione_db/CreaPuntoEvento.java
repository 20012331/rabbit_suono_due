package funzione_db;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.cloud.FirestoreClient;
import contenitori.EqSegmento;
import contenitori.PuntoEvento;
import partenza.Costanti;
import sensore.ConnessioneSensore;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CreaPuntoEvento implements Runnable {
	
	// 1km in gradi = 1 / 111.32km = 0.0089
	// 1m in gradi = 0.0089 / 1000 = 0.0000089
	//double coef = meters * 0.0000089;
	private static final double COEF_CORREZIONE = 0.0000089;
	private static final double LUNGHEZZA_SEGMENTO = 150;
	// pi / 180 = ~0.0174 -> 0.018
	private static final double GRADO_AGGIUSTATO = 0.018;
	
	private long idMilli;
	
	private static Firestore db = FirestoreClient.getFirestore();
	
	
	public CreaPuntoEvento( long idMilli ) {
		this.idMilli = idMilli;
	}

	/**
	 * recupero le rette dal db e creo un contenitore di segmenti
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	public void recuperaRette() throws InterruptedException, ExecutionException {

		EqSegmento eqSeg;
		ArrayList<EqSegmento> eqList = new ArrayList<>(); 
		
		Query q = db.collection( Costanti.FB_DOC_RETTE_SUONI ).whereEqualTo("id", idMilli );
		
		ApiFuture<QuerySnapshot> fut = q.get();
				
		List<QueryDocumentSnapshot> lista = fut.get().getDocuments();
		
		for( QueryDocumentSnapshot doc : lista ) {
			// ottengo la retta
			eqSeg = doc.toObject(EqSegmento.class);
			
			impostaFineSegmento( eqSeg );
						
			// aggiungo il segmento al gruppo
			eqList.add(eqSeg);
		}
				
		cercaIntersezione(eqList);
	}
	
	/**
	 * calcolo i punti finali del segmento
	 * @param segmento
	 */
	public void impostaFineSegmento( EqSegmento segmento ) {
				
		// definisco la parte finale del segmento
		// la lunghezza delle x,y
		double y_end = Math.sin( segmento.rad ) * LUNGHEZZA_SEGMENTO;
		double x_end = Math.cos( segmento.rad ) * LUNGHEZZA_SEGMENTO;
		
		// applico la lunghezza alla posizione
		segmento.lat_y = segmento.lat + ( y_end * COEF_CORREZIONE);
		segmento.lng_x = segmento.lng + ( x_end * COEF_CORREZIONE) / Math.cos(segmento.lat * GRADO_AGGIUSTATO);
				
	}
	
	/**
	 * dati i segmenti cerco un possibile intersezione
	 * @param lista
	 */
	public void cercaIntersezione( List<EqSegmento> lista ) {
		PuntoEvento pe = new PuntoEvento();
		EqSegmento segmento = null;
			
		if( lista.size() == 1 ) {
			
			segmento = lista.get(0);
			pe = intersezioneMono(segmento);
			// completo quello che manca
			pe.tipoSuono = segmento.tipoSuono;
			pe.id = segmento.id;
			
		}else if ( lista.size() > 1 ) {
			
			segmento = lista.get(0);
			pe = intersezioneMulti(lista);
			pe.tipoSuono = segmento.tipoSuono;
			pe.id = segmento.id;
			
		}
		
		//System.out.println( pe.toString() );
		db.collection( Costanti.FB_DOC_EVENTO ).add( pe );
	}
	
	/**
	 * cerco il punto di intersezione fra piu rette
	 * @param lista
	 * @return
	 */
	public PuntoEvento intersezioneMulti( List<EqSegmento> lista ) {
		PuntoEvento pe = new PuntoEvento();
		
		EqSegmento r0 = lista.get(0);
		EqSegmento r1 = null;//lista.get(1);
		
		PuntoEvento pe2 = intersezioneMono(r0);
		
		// ottengo la retta
		r0.m = ( r0.lat_y - r0.lat ) / ( r0.lng_x - r0.lng );
		//r0.q = ( r0.lng_x * r0.lat ) - ( r0.lng * r0.lat_y ); r0.q = r0.q / ( r0.lng_x - r0.lng );
		r0.q = -1 * (r0.m * r0.lng ) + r0.lat;
			
		for ( int i = 1 ; i < lista.size() ; i = i + 1) {
			r1 = lista.get(1);
			// ottengo la retta
			r1.m = ( r1.lat_y - r1.lat ) / ( r1.lng_x - r1.lng );
			//r1.q = ( r1.lng_x * r1.lat ) - ( r1.lng * r1.lat_y ); r1.q = r1.q / ( r1.lng_x - r1.lng );
			r1.q = -1 * ( r1.m * r1.lng ) + r1.lat; 
			
			// qui dovrei vedere l'intersezione
			pe.lng = ( r1.q - r0.q) / ( r0.m - r1.m);
			pe.lat = r0.m * pe.lng + r0.q;
			
			// gestisco i casi di non intersezione
			// NON gestisco quelli errati
			if( Double.isFinite( pe.lng ) && Double.isFinite( pe.lat ) ) {
				i = lista.size();
			} else { 
				if( ( lista.size() - 1 ) == i ) { pe = pe2; }
			}
			
		}
		
		
		
		return pe;
	}
	
	/**
	 * se ho un solo segmento prendo il punto medio
	 * @param segmento
	 * @return
	 */
	public PuntoEvento intersezioneMono( EqSegmento segmento ) {
		PuntoEvento pe = new PuntoEvento();
		pe.lat = ( segmento.lat_y + segmento.lat ) / 2;
		pe.lng = ( segmento.lng_x + segmento.lng ) / 2;
		return pe;
	}
		
	@Override
	public void run() {
		
		try {
			// attendo del tempo per essere certo di avere piu di una retta disponibile
			// nel db
			Thread.sleep( Costanti.THREAD_PAUSA );
			recuperaRette();
		} catch ( InterruptedException | ExecutionException ex ) {
			// avviso che ho avuto un problema
			Logger.getLogger(ConnessioneSensore.class.getName()).log( Level.SEVERE , null , ex );
		}

	}

}
