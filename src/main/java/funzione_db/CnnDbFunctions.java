/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funzione_db;

import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentChange;
import com.google.cloud.firestore.DocumentChange.Type;
import com.google.cloud.firestore.EventListener;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreException;
import com.google.cloud.firestore.ListenerRegistration;
import com.google.cloud.firestore.Query.Direction;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.cloud.FirestoreClient;


import contenitori.PuntoEvento;
import partenza.Costanti;
import funzione_db.AttivaFunzione;

/**
 *
 * @author universita
 */
public class CnnDbFunctions {

  

    private static long ultimoId = 0;
    private static Firestore db = FirestoreClient.getFirestore();
    
    private Thread t = null;
    private AttivaFunzione fPerDb;
    private ListenerRegistration lrRetta;
    
    public CnnDbFunctions(){ }
    

    
    
	/**
	 * recupero dal db l'ultimo valore inserito , oppure uso quello di una 
	 * settimana fa . In caso di errore uso quello di una settimana fa
	 */
	private void recuperaUltimoId() {
		// la settimana scorsa come valore base
		ultimoId = System.currentTimeMillis() - ( 3600 * 24 * 7 * 1000);
		
		// recupero ultima valore da db
		ApiFuture<QuerySnapshot> fut =  db.collection( Costanti.FB_DOC_RETTE_SUONI)
		.orderBy(PuntoEvento.CAMPO_ID, Direction.DESCENDING )
		.limit( 1 ).get();
		
		try {
			fut.get().getDocuments().forEach( (QueryDocumentSnapshot doc)->{
				ultimoId = doc.getLong( PuntoEvento.CAMPO_ID );
			});
		} catch (InterruptedException | ExecutionException e) {
			
		}
		
	} 

	private void ascoltaEventi() {
		eventiRetta();		
		
	} 
	
	
	private void eventiRetta() {
		// recupero ultima valore da db
		lrRetta = db.collection(Costanti.FB_DOC_RETTE_SUONI)
			.whereGreaterThan(PuntoEvento.CAMPO_ID, ultimoId)
			.addSnapshotListener( nuovaRetta );
	}
	
	/** ogni notifica di nuova retta controllo la data e poi elaboro */
    private EventListener< QuerySnapshot > nuovaRetta = new EventListener<>() {

		@Override
		public void onEvent(QuerySnapshot value, FirestoreException error) {

			// se errore esci
			if (error != null) {
				System.err.println("Listen failed: " + error);
				return;
			}


			// se e' un nuovo documento
			for (DocumentChange dc : value.getDocumentChanges()) {
				if (dc.getType() == Type.ADDED) {

					// metti i ncoda ogni valore
					value.forEach((QueryDocumentSnapshot doc) -> {

						try {
							AttivaFunzione.coda.put(doc.getLong(PuntoEvento.CAMPO_ID));
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

					});

				}
			}

		}
	}; 
	
	
    /* TODO : leggi lista qui sotto
     * 
     * fatto
     * 1) decidere se connettersi al db o rimanere in ascolto sul broker
     * 		-- confermo ascolto il db on line , elaboro in locale e spedisco il risultato
     * 		- sarebbe meglio creare un processo in ascolto sul broker , ma 
     * 			disaccoppio e rimango in ascolto sul db.
     * 
     * fatto
     * 2) gestire i multiriga
     * 		- ho gestito le righe che non mi interessano. Chi non e' stato
     * 			creato, non viene considerato
     * 
     * fatto
     * 3) calcolare l'intersezione 
     * 		- la calcola il thread dedicato
     * 
     * 
     * - la coda blocca tutto il sistema, cercare di finire il test
     * -- RISOLTO , usavo il metodo errato per attivare il thread.
     * 
     * 4) spedire al db il punto di origine
     * 
     * 
     * 5) comunicare che c'e un evento disponibile o eseguire il pool da web 
     * 		magari prevedo un secondo thread che si occupi solo di questo
     * 		magari lo metto dentro al server web ... ci provero'
     * 		- sposterei l'onere al web server , perche e' lui che distribuisce 
     * 			i contenuti. Quindi, credo che deleghero' : 1) ascolto per
     * 			nuovi eventi , 2) distribuzione eventi , 3) richieste recupero
     * 			eventi passati  
     */
     
    
    
    /**
     * LA PRIMA PARTE E BLOCCANTE : recupera i dati e inizia a lavorare
     */
    public void attiva(){

    	// inizializzo i dati
    	recuperaUltimoId();
    	ascoltaEventi();
    	
    	// funzioni per il calcolo
    	fPerDb = new AttivaFunzione(ultimoId);
    	t = new Thread(fPerDb , "funzione per il db");  
    	t.setDaemon(false);
    	t.start();
        
    }
    
    /** chiamarlo per scrivere su file le impostazioni
     * @return  le impostazione del sensore
     */
    public void disattiva() {
    	
    	lrRetta.remove();
    	fPerDb.quit();
    	
    	
    	
    }
   
    
    
}
