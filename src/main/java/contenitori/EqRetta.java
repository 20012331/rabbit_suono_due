package contenitori;

public class EqRetta extends PuntoEvento{
		

	public double rad = 0;
    
	public String nSensore = "";
    // questi avrei potuto eliminarli 
    /** pendenza */
	public double m = 0;
	/** scostamento */
	public double q = 0;
	// questi avrei potuto eliminarli ^
	
	public EqRetta() {}

	public EqRetta(String tipoSuono, String nSensore, double lng, double lat, double rad, long id, double m, double q) {
		super();
		this.tipoSuono = tipoSuono;
		this.nSensore = nSensore;
		this.lng = lng;
		this.lat = lat;
		this.rad = rad;
		this.id = id;
		this.m = m;
		this.q = q;
	}

	@Override
	public String toString() {
            return "EqRetta [m=" + m + ", q=" + q + ", rad=" + rad + ", tipoSuono=" + tipoSuono + ", nSensore=" + nSensore + ", lon=" + lng
                + ", lat=" + lat + ", id=" + id + "]";
	}
	
	
	
}
