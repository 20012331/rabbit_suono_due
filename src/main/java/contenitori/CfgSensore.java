package contenitori;

public class CfgSensore extends CfgChannel {
	/** tempo di riannuncio in secondi */
	public static final int TEMPO_ANNUNCIO = 15;

	public static final String PAR_STATO_ATTIVO = "on";
	public static final String PAR_STATO_PASSIVO = "off";

	public String nSensore = null;
	/** latitudine = y */
	public double lat;
	/** longitudine = x */
	public double lng;
	public String stato = "";
	public String log = "";


        
	// "param" : { "lat": , "lng" : , "stato" = "on" }

}
