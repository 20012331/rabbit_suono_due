package contenitori;

public class RispostaOperatore {

    public static final int APERTO = 1;
    public static final int CHIUSO = 99;

    public static final String[] nomiOp = {
            "Vigilante",
            "Polizia",
            "Carabinieri"
    };

    /** il medesimo del suono */
    public long id;

    /** ora in cui ho risposto*/
    public long oraRisposta;

    /** Y della mia pos */
    public double lat;

    /** X della mia pos */
    public double lng;

    /** nome di chi ha risposto */
    public String nomeOp = nomiOp[0];

    public String docId = "";

    /**
     * 1 = aperto
     * 99 = chiuso
     * */
    public int stato = APERTO;


    public RispostaOperatore(){}

    @Override
    public String toString() {
        return "RispostaOperatore{" +
                "id=" + id +
                ", oraRisposta=" + oraRisposta +
                ", lat=" + lat +
                ", lng=" + lng +
                ", nomeOp='" + nomeOp + '\'' +
                ", docId='" + docId + '\'' +
                ", stato=" + stato +
                '}';
    }
}
