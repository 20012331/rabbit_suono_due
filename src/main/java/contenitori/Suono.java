package contenitori;

public class Suono {
	
	public String nSensore = "";
	public String tipoSuono = "";
	public double tempoMicDX = 0;
	public double tempoMicSX = 0;
	public double tempoMicUP = 0;
	public long milli = 0;
	
	// serve vuoto per Gson
	public Suono() {}

	@Override
	public String toString() {
		return "Suono [nSensore=" + nSensore + 
				", tipoSuono=" + tipoSuono + 
				", tempoMicDX=" + tempoMicDX + 
				", tempoMicSX=" + tempoMicSX + 
				", tempoMicUP=" + tempoMicUP + "]";
	}




}
