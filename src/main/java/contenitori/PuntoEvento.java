package contenitori;

public class PuntoEvento {

	public static final String CAMPO_ID = "id";
	
	public String tipoSuono = "";
	
	//public String nSensore = "";
	
	/** la x */
	public double lng = 0;
	/** la y */
	public double lat = 0;
	
    /** i millisecondi */ 
	public long id;

	public PuntoEvento() {	}



	@Override
	public String toString() {
		return "PuntoEvento [tipoSuono=" + tipoSuono + ", lng=" + lng + ", lat=" + lat
				+ ", id=" + id + "]";
	}
	
	
}
