package contenitori;

public class Messaggio {

	/*
	 * comandi che potrei ricevere
	 * 		AVVISARE SEMPRE RIGUARDO L'ESITO
	 * 		stato = restituisco nome e posizione
	 * 		n_posizione = la nuova posizione del sensore (no riavvio )
	 * 
	 * da fare dopo 
	 * 		up = riattivo i messaggi
	 * 		down = blocco i messaggi 
	 *  
	 * 
	 * possibili e da valuare
	 * restart = spegne e riaccende con i nuovi parametri
	 * config = cambia i parametri di configurazione
	 */

	public static final String TUTTI_SENSORI = "tutti";
	public static final String CMD_STATO = "stato";
	//public static final String CMD_SET_STATO = "set_stato";
	// rispondere con stato dopo aver aggiornato la posizione
	public static final String CMD_NEW_POS = "n_posizione";
    public static final String CMD_NEW_NOME = "n_nome";

    
    public static final String CMD_AREA_SUONO = "area_suono";
    public static final String CMD_SEGMENTO_SUONO = "segmento_suono";
    public static final String CMD_SUONO = "suono";
    public static final String CMD_LOG_ON = "log_on";
    public static final String CMD_LOG_OFF = "log_off";
    public static final String CMD_LOG_LIST = "log_list";
    public static final String CDM_NEW_CFG = "new_cfg";


    public static final String CMD_NEW_SENSORE = "n_sensore";
    
    /** avvisa che c'e una o piu rette da elaborare */
    public static final String CMD_ELABORA_RETTA = "elabora_retta";
    public static final String CMD_PUNTO_EVENTO = "punto_evento";
    /** nome da usare per il processo di elaborazione */
    public static final String NSENSORE_FUNZIONE = "funzione";
    public static final String CMD_NSENSORE_MANAGER = "manager";



    // comandi per il canale
    public static final String CMD_CAMBIO_CITTA = "new_citta";
	public static final String CMD_CAMBIO_EX_CMD = "new_ex_cmd";
	public static final String CMD_CAMBIO_EX_SPED = "new_ex_spd";
	public static final String CMD_QUIT = "quit";

	/** richiesta ultimi dieci eventi del db */
	public static final String CMD_INIT_EVENTO_SUONO = "init_evento";

	//public static final String PAR_LAT_Y = "lat : ";
	//public static final String PAR_LNG_X = "lng : ";


	public String nSensore = "";
	public String cmd = "";
	// esempio di risposta 
	// cambio posizione 
	// { lat:22.22 , lon : 22.22 } 
	//public Param param = new Param();
	public CfgSensore cfgSensore = null;
	public Suono suono = null;
	public PuntoEvento puntoEvento = null;
	public RispostaOperatore rispostaOperatore = null;
	public CfgChannel cfgChannel = null;
	// TODO : valutare se tenere in una classe sola
	//public Suono suono = new Suono();

	public Messaggio() {}

        
        //"param" : { "lat": , "lng" : , "stato" = "on" }
}
