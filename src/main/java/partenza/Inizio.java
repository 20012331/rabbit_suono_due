/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package partenza;

import funzione_db.CnnDbFunctions;
import sens_factory.CnnManager;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import sensore.ConnessioneSensore;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import optSuFile.OperazioniSuFile;
import restServer.RestServer;


/**
 *
 * @author universita
 */
public class Inizio {


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    	
    	// inizializzo firebase
        inizializzaFirebase();

        // comincio a creare il server
        RestServer server = new RestServer();
        server.attiva();

        // longi = x , lat = y
        //Sensore s = new Sensore(  , 44.92271 , Costanti.NOME_SENSORE );

        // la funzione che elabora il db
        CnnDbFunctions funzioneDb = new CnnDbFunctions();
        funzioneDb.attiva();

        // chi ascolta per creare nuovi sensori ...
        CnnManager cnnMan = null;
        cnnMan = new CnnManager();
        cnnMan.attiva();



        // lancio i sensori se esistono file di cfg
        List< String > nomiFileCfg = OperazioniSuFile.listaNomiFileSensori();
        for (String nomeFile : nomiFileCfg) {
            ConnessioneSensore conSen = new ConnessioneSensore( nomeFile );
            conSen.attiva();
        }



        /*Logger.getLogger( Inizio.class.getName() ).log(
            Level.SEVERE,
            "Impossibile raggiungere il server, programma chiuso "
        );*/

		System.out.println( "chiusura main" );

    }


    /**
     * inizializzo firebase con i dati presi dal file json preso da google.
     */
    public static void inizializzaFirebase() {
        try {
            // Use a service account
            InputStream serviceAccount = new FileInputStream("provafiredb-a8734d238316.json");
            GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);
            FirebaseOptions options = new FirebaseOptions.Builder().setCredentials(credentials).build();
            FirebaseApp.initializeApp(options);

        } catch (IOException ex) {
                Logger.getLogger(Inizio.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}