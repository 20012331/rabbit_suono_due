/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package partenza;

import contenitori.CfgSensore;
import org.jetbrains.annotations.NotNull;

/**
 *
 * @author universita
 */
public class Costanti {
    
//    public static final String USER_NAME = "guest";
//    public static final String PASSWORD = "guest";
//    public static final String VIRTUAL_HOST= "/";
//    public static final String HOST_NAME = "172.17.0.2";
//    public static final int NUMERO_PORTA = 5672;    
//
//    /** questa e la routing queue */
//    //public static final String NOME_CODA = "prova_coda";
//    //public static final String[] ROUTING_KEY_ARR = new String[]{"info","warn","error"};
//    public static final String NOME_EXCHANGE_CMD = "ex_comandi";
//    public static final String ROUTNG_KEY_CMD = "italia.piemonte.al.al";
//
//    public static final String NOME_EXCHANGE_SUONI = "ex_suoni";
//    public static final String ROUTNG_KEY_SUONI = "suono";
//    
//    public static final String NOME_EXCHANGE_SPEDIZIONE = "ex_spedizione";
//    public static final String ROUTNG_KEY_SPEDIZIONE = "spedizione";
    
    
    //public static final String NOME_SENSORE = "s0";
    
    // misure non veritiere
    public static final double GEO_1_CM = 0.000000125;
    public static final double GEO_5_CM = GEO_1_CM * 5;
    public static final double GEO_1_M = GEO_1_CM * 100;
    
	public static final String FB_DOC_RETTE_SUONI = "retta";
	public static final String FB_DOC_EVENTO = "puntoEvento";
	
	/** pausa di tre secondi = 3000 milli*/
	public static final int THREAD_PAUSA = 3000;
	/** lunghezza coda */
	public static final int CODA_DIM_ID = 30;
    
    public static final double LAT_NEW_SENSORE = 44.923307;
    public static final double LNG_NEW_SENSORE = 8.613855;

    public static final String DEF_ITALIA = "it";
    public static final String DEF_CITTA = "alessandria";
    public static final String DEF_EX_CMD = "ex_comandi";
    public static final String DEF_EX_SPD = "spedizione";
    public static final String DEF_EX_SND = "ex_suoni";
    public static final String DEF_RK_SND = "suono";

    public static final int DEF_RIGHE_LOG = 10;

    // geoindicazioni universita
    // lat , lng
    // universita
    // 44.92271 , 8.616967 
    // 44.922486 , 8.618308
    // 44.922133 , 8.616768
    // 44.921863 , 8.618066


    public String NOME_EXCHANGE_CMD = "ex_comandi";
    public String ROUTNG_KEY_CMD = "italia.piemonte.al.al";

    public String NOME_EXCHANGE_SUONI = "ex_suoni";
    public String ROUTNG_KEY_SUONI = "suono";

    public String NOME_EXCHANGE_SPEDIZIONE = "ex_spedizione";
    public String ROUTNG_KEY_SPEDIZIONE = "spedizione";



    
    
    //config rest server
    public static final int REST_PORT = 13443;
    public static final String REST_LINK = "/rest";
    public static final String REST_HELP = "help";

    public static final String REST_CMD_STATO  = "stato";
    public static final String REST_CMD_CITTA  = "citta";
    public static final String REST_CMD_ON  = "on";
    public static final String REST_CMD_OFF  = "off";
    public static final String REST_CMD_QUIT = "quit";
    // sinonimi Y
    public static final String REST_CMD_LAT = "lat";
    public static final String REST_CMD_LATITUDE = REST_CMD_LAT + "itude";
    public static final String REST_CMD_LATITUDINE = REST_CMD_LAT + "itudine";
    public static final String REST_CMD_Y = "y";
    // sinonimi X
    public static final String REST_CMD_LNG = "lng";
    public static final String REST_CMD_LONGITUDE = "longitude";
    public static final String REST_CMD_LONGITUDINE = "longitudine";
    public static final String REST_CMD_X = "x";

    public static final String REST_CMD_POS = "pos";
    public static final String REST_CMD_POSIZIONE = "posizione";

    public static final String REST_CMD_LOG = "log";

    public static final String REST_CMD_NRIGHE = "righe";
    public static final String REST_SENSORI = "sensori";
    //public static final String REST_SENSORI = "sensori";
    //public static final String REST_SENSORI = "sensori";


    public static final String TXT_HELP =
        "Help : struttura comandi \n" +
        "---------------------------------------- \n" +
        "get : \n" +
        REST_LINK + "/" + REST_HELP + "\n" + "\t questo testo \n" +
        REST_LINK + "/citta/sensore \n" +
        REST_LINK + "/citta/sensore/proprieta \n" +
        "\t restituiscono sempre la configurazione \n" +
        "\t del sensore o del gruppo di sensori oppure \n" +
        "\t la proprieta richiesta , se esite. \n" +
        "---------------------------------------- \n" +
        "put : \n" +
        REST_LINK + "/citta/sensore + cfgJson \n" +
        REST_LINK + "/citta/sensore/proprieta/valore \n" +
        "\t impostano la proprieta al valore specificato \n" +
        "\t tutti restituiscono 200 (successo) o 404 \n" +
        "\t (errori vari )\n"+
        "---------------------------------------- \n" +
        "cfgJson : " +
        "{\n" +
        "    \"citta\": \"alessandria\",\n" +
        "    \"exCmd\": \"ex_comandi\",\n" +
        "    \"lat\": 44.92271,\n" +
        "    \"lng\": 8.616967,\n" +
        "    \"log\": \"on\",\n" +
        "    \"nSensore\": \"s0\",\n" +
        "    \"rkSpd\": \"spedizione\",\n" +
        "    \"stato\": \"on\"\n" +
        "}\n" +
        "valori di esempio , alcune proprieta possono essere omesse \n" +
        "---------------------------------------- \n";



    @NotNull
    public static CfgSensore deepCopy(@NotNull CfgSensore orig){
        CfgSensore dest = new CfgSensore();

        dest.stato = orig.stato;
        dest.nSensore = orig.nSensore;
        dest.lng = orig.lng;
        dest.lat = orig.lat;
        dest.log = orig.log;

        dest.citta = orig.citta;
        dest.exCmd = orig.exCmd;
        dest.rkSpd = orig.rkSpd;

        return dest;
    }
}
