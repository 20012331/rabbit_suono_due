/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package optSuFile;

import com.google.gson.Gson;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import contenitori.CfgConnessione;
import contenitori.CfgSensore;
import partenza.Costanti;
import partenza.Inizio;

/**
 *
 * @author universita
 */
public class OperazioniSuFile {

    private static final Path DIR_CONNESSIONE = Paths.get("cfgConnessione");
    private static final Path DIR_SENSORI = Paths.get("cfgSensori");
    private static final Path DIR_LOGS = Paths.get("logs");
    
    
    //public OperazioniSuFile() {}
    
    /* *************************************************************************
    scrittura file 
    ************************************************************************* */
    public static void scriviFileConnessione( String nomeFile , String stringaJson ){
        scriviSuFile( DIR_CONNESSIONE , nomeFile, stringaJson);
    }
    public static void scriviSensore( String nomeFile , String stringaJson ){
        scriviSuFile( DIR_SENSORI , nomeFile, stringaJson);
    }
    public static void scriviCfgSensore( String nomeFile , String stringaJson ){
        scriviSuFile( DIR_SENSORI , nomeFile, stringaJson);
    }

    private static void scriviSuFile(Path dir , String nomeFile , String stringaJson ){
        BufferedWriter bw= null;
        File nf = new File( dir.toAbsolutePath().toString() + FileSystems.getDefault().getSeparator() + nomeFile );

        try {
            
            if( Files.notExists(DIR_SENSORI) ){
                Files.createDirectory(DIR_SENSORI);
            }
        
            bw = Files.newBufferedWriter( nf.toPath() );
            bw.write( stringaJson );
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger( Inizio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    
    
    /* *************************************************************************
    lettura file 
    ************************************************************************* */
       
    public static String leggiLog( String nomeFile , int nRighe ) throws IOException {
    	String testo = null;
    	File nf = new File( DIR_LOGS.toAbsolutePath().toString() + FileSystems.getDefault().getSeparator() + nomeFile );
    	
    	
    	StringBuilder sb = new StringBuilder();
    	RandomAccessFile rf = new RandomAccessFile( nf , "r");
    	long lungByte = rf.length() - 1;
    	int nrLette = nRighe;
    	byte leggo;
    	
    	for( long car = lungByte; ( ( car > 0) && ( nrLette >= 0 ) ) ; car = car -1 ) {
    		rf.seek(car);
    		leggo = rf.readByte();
    			
    			if( leggo == '\n') {
    				nrLette = nrLette - 1;
    			}
    			
    		sb.append( (char)leggo );
    		
    	}
    	
    	testo = sb.reverse().toString();
    	return testo;
    }
    
    
    
    public static String leggiFile( Path dir , String nomeFile ) throws IOException{
        
        String testo = null;
        File nf = new File( dir.toAbsolutePath().toString() + FileSystems.getDefault().getSeparator() + nomeFile );
        testo =  Files.readString( nf.toPath() );
       
        return testo;
    }
    
    /**
     * ritorna sempre qualcosa di consistente
     * @return i valori letti dal file di configurazione o quelli di base
     */
    public static CfgConnessione leggiFileConnessione(){
        CfgConnessione cfg = null;
        
        
        try {
            cfg =  new Gson().fromJson( 
                leggiFile(DIR_CONNESSIONE, CfgConnessione.cfgNomeFile ), 
                CfgConnessione.class
            );
            
        } catch (IOException ex) {
            Logger.getLogger(OperazioniSuFile.class.getName()).log(Level.INFO, "errore lettura " + CfgConnessione.cfgNomeFile + "utilizzo valori base");
            
            // in caso andasse storto
            cfg = new CfgConnessione();
        }
        return cfg;
        
    }
    
    
    /**
     * 
     * @param nomeFile
     * @return null se ci sono stati errori
     */
    public static CfgSensore leggiFileSensore( String nomeFile ){
        CfgSensore cfg = null;
        
        //File nf = new File( DIR_SENSORI.toAbsolutePath().toString() + FileSystems.getDefault().getSeparator() + CfgConnessione.cfgNomeFile );
        
        try {
            cfg =  new Gson().fromJson( 
                leggiFile(DIR_SENSORI, nomeFile ), 
                CfgSensore.class
            );
                        
        } catch (IOException ex) {
            Logger.getLogger(OperazioniSuFile.class.getName()).log(Level.INFO, "errore lettura " + nomeFile + " sensore non attivato" );
            Logger.getLogger(OperazioniSuFile.class.getName()).log(Level.SEVERE, null, ex);
            
            // in caso qualcosa vada male
            // TODO : mettere dei valori base e dare la possibilita di riprovare
            cfg = null;
        }
        return cfg;
    }

    /**
     * leggo il file di configurazone del sensore
     * @param //nomeFile
     * @return null se ci sono stati errori
     */
    /*public static CfgFile leggiFileCfgSensore( String nomeFile ){
        CfgFile cfg = null;

        //File nf = new File( DIR_SENSORI.toAbsolutePath().toString() + FileSystems.getDefault().getSeparator() + CfgConnessione.cfgNomeFile );

        try {
            cfg =  new Gson().fromJson( leggiFile(DIR_SENSORI, nomeFile ), CfgFile.class );

        } catch (IOException ex) {
            Logger.getLogger( OperazioniSuFile.class.getName() ).log( Level.SEVERE, "errore lettura " + nomeFile + " sensore non attivato" );
            //Logger.getLogger(OperazioniSuFile.class.getName()).log(Level.SEVERE, null, ex);

            // in caso qualcosa vada male
            cfg = null;
        }
        return cfg;
    }*/
    
    
    
    
    
    /* *************************************************************************
    lettura e scrittura di gruppo
    ************************************************************************* */
    
    public static Map<String , CfgSensore > leggiTuttiFileSensori(){
        
        HashMap< String , CfgSensore > mappa = new HashMap<>();
        CfgSensore cfg = null;
        
        try ( DirectoryStream<Path> stream = Files.newDirectoryStream( DIR_SENSORI ) ) {
            for (Path file: stream) {
                cfg = leggiFileSensore( file.getFileName().toString()  );
                if( cfg != null ){
                    // TODO : magari mettere un valore base e riprovare la lettura
                    mappa.put(file.getFileName().toString(), cfg);
                }
            }
        } catch (IOException | DirectoryIteratorException x) {
            // IOException can never be thrown by the iteration.
            // In this snippet, it can only be thrown by newDirectoryStream.
            System.err.println(x);
            mappa = null;
            Logger.getLogger(OperazioniSuFile.class.getName()).log(Level.INFO, "Non e possibile leggere i files di configurazione dei sensori" );
        }
        return mappa;
    }
    public static List<String > listaNomiFileSensori(){

        List< String > lista = new LinkedList<>();

        try ( DirectoryStream<Path> stream = Files.newDirectoryStream( DIR_SENSORI ) ) {
            for (Path file: stream) {
                lista.add( file.getFileName().toString() );
            }
        } catch (IOException | DirectoryIteratorException x) {
            // IOException can never be thrown by the iteration.
            // In this snippet, it can only be thrown by newDirectoryStream.
            lista = null;
            Logger.getLogger(OperazioniSuFile.class.getName() ).log( Level.SEVERE , null , x );
            Logger.getLogger(OperazioniSuFile.class.getName() ).log(Level.SEVERE, "Non e possibile leggere i files di configurazione dei sensori" );
        }
        return lista;
    }
    
    public static void scriviTuttiFileSensori( Map<String , CfgSensore > mappa){
        // modifica proposta dallo editor
        // for( Map.Entry<String , ParamCfgSensore> coppia :  mappa.entrySet() ) {
        
        mappa.entrySet().forEach( (coppia) -> {
            scriviSensore(coppia.getKey(), new Gson().toJson( coppia.getValue() ) );
        });
    }
    
    
    /**
     * 
     * @return null in caso di errori
     */
    public static String nomeNuovoSensore(){
        
        String nome = null;
        int nMaggiore = -1;
        
        try ( DirectoryStream<Path> stream = Files.newDirectoryStream( DIR_SENSORI ) ) {
            
        	for (Path file: stream) {
                nome  = file.getFileName().toString();
            	nMaggiore = Math.max( 
        			nMaggiore ,
        			Integer.parseInt( nome.substring(1) )
    			);
            	   
            }
            
        } catch (IOException | DirectoryIteratorException x) {
            // IOException can never be thrown by the iteration.
            // In this snippet, it can only be thrown by newDirectoryStream.
            nMaggiore = -1;
            nome = null;
            Logger.getLogger(OperazioniSuFile.class.getName()).log(Level.INFO, "Errore lettura elenco file config sensori" );
        }
        
        // dovrebbe essere inutile in ambiente contrpllato
        if( nMaggiore < 0 ) { nome = null; }
        // questa parte la creo dopo  
        else { nome = "s" + (nMaggiore + 1); }
        
        return nome;
    }
    
    
    
    
    
    
    /* *************************************************************************
    cancello file inutili
    ************************************************************************* */
    public static void cancellaFileSensore( String nomeFile ){
        File nf = new File( DIR_SENSORI.toAbsolutePath().toString() + FileSystems.getDefault().getSeparator() + nomeFile );
        try {
            Files.deleteIfExists( nf.toPath() );
        } catch (IOException ex) {
            Logger.getLogger(OperazioniSuFile.class.getName()).log(Level.SEVERE, null, ex);
            Logger.getLogger(OperazioniSuFile.class.getName()).log(Level.SEVERE, null, "eliminazione non riuscita file : " + nomeFile );
        }
    }










    /* *************************************************************************
    creo il sensore
    ************************************************************************* */

    /**
     * se il sensore viene scritto lo posso anche avviare
     * @return null in caso di errori
     */
    public static CfgSensore creaCfgNuovoSensore() {

        Gson gson = new Gson();

        // impostazioni base
        CfgSensore cfg = new CfgSensore();

        // ottengo un nome libero
        cfg.nSensore = OperazioniSuFile.nomeNuovoSensore();


        if( cfg.nSensore != null ) {

            // imposto valori base
            cfg.stato = CfgSensore.PAR_STATO_PASSIVO;
            cfg.lat = Costanti.LAT_NEW_SENSORE;
            cfg.lng = Costanti.LNG_NEW_SENSORE;

            cfg.citta = Costanti.DEF_CITTA;
            cfg.exCmd = Costanti.DEF_EX_CMD;
            cfg.rkSpd = Costanti.DEF_EX_SPD ;


            // memorizzo la configurazione
            OperazioniSuFile.scriviCfgSensore( cfg.nSensore , gson.toJson(cfg) );
            // leggo la configurazione appena scritta
            cfg = OperazioniSuFile.leggiFileSensore( cfg.nSensore );

        }
        return cfg;
    }






}
