server.post( "/:zona/:sens", (ctx) -> {

			String zona = ctx.pathParam("zona");
			String sens = ctx.pathParam("sens");

			HashMap<String, String> map= new HashMap<>();
			map.put( "zona" , zona);
			map.put( "sens" , sens);
			map.put( "body" , ctx.body() );
			//map.put( "att2" , ctx.attribute("uno") );
			ctx.json( map );

		});

		/*
		 * serve solo per ottenere le info basilari su un sensore o sui gruppi
		 * /al/sensori
		 * /al/s11
		 *
		 */
		server.get( "/:zona/:sens", (ctx) -> {

			String s = ctx.pathParam("sens");
			ConnessioneSensore cs = mapSensori.getOrDefault(s, null);

			// se chiedo tutto
			if( s.equalsIgnoreCase( Costanti.REST_SENSORI ) ) {

				HashMap<String , ParamCfgSensore > sensori = new HashMap<>();
				mapSensori.entrySet().forEach( (riga) -> {
					sensori.put(riga.getKey() , riga.getValue().getParamSensore() );
				});
				ctx.json( sensori );

			// se invece e' uno solo
			} else if (cs != null ) {
				ctx.json( cs.getParamSensore() );

			} else {
				//TODO : errore
				ctx.result("errore");

			}

		});

		// vedere
		// https://www.w3.org/Protocols/rfc2616/rfc2616.html
		// sezione 9 ...
		//
		//sembra che basta solo la risposta http
		// 200 ok
		// 204 mo contenent
		// il nuovo link in location
		//
		// per il post
		// 201 created
		//
		// cambio uno o piu parametri
		/*
		 * modifico uno o piu valori e ottengo il nuovo contenuto
		 * /al/s123 { "lat":"22" , "lng" = "8" , "stato":"off"}
		 * /al/s123 { "stato":"off"}
		 *
		 * risposta : { "lat":"22" , "lng" = "8" , "stato":"off"}
		 */
		server.put( "/:zona/:sens", (ctx) -> {

			// se ci sono problemi di conversione risponde con un messaggio
			// in automatco
			ConnessioneSensore cs = mapSensori.getOrDefault( ctx.pathParam( "sens" ), null);
			if( cs != null ) {

				Messaggio cmd = new Messaggio();
				CfgSensore param = ctx.bodyAsClass( CfgSensore.class );
				ParamCfgSensore pSens = cs.getParamSensore();

				// prima la posizione
				if( param.lat == 0 ) { param.lat = pSens.lat; }
				if( param.lng == 0 ) { param.lng = pSens.lng; }
				cmd.param.lat = param.lat;
				cmd.param.lng = param.lng;
				cs.cambioPosizione(cmd);


				// poi lo sato
				if( ! // se non e' così
					( param.stato.equalsIgnoreCase(Costanti.REST_CMD_ON)	||
					param.stato.equalsIgnoreCase(Costanti.REST_CMD_OFF) )
				){
					cmd.cmd = pSens.stato;
					param.stato = pSens.stato;
				} else { cmd.cmd = param.stato; }
				cs.cambioStatoSensore(cmd);

				ctx.json( (CfgSensore)cs.getParamSensore() );
				//ctx.result("fatto");
			}

		});






//		server.put( "/:zona/:sens/:cmd", (ctx) -> {
//
//			String z = ctx.pathParam("zona");
//			String s = ctx.pathParam("sens");
//			String c = ctx.pathParam("cmd");
//
//			ConnessioneSensore cs = mapSensori.getOrDefault(s, null);
//			Comando cmd = new Comando();
//			cmd.cmd = c;
//
//			try { Double.parseDouble( ctx.body() );}
//			catch ( NullPointerException | NumberFormatException ex )
//			{cs = null;}
//
//			if ( cs != null ) {
//
//				Param param = (Param)cs.getParamSensore();
//
//
//				switch ( c.toLowerCase() ) {
//
//					// attiva / addormenta
//					case Costanti.REST_CMD_ON:
//					case Costanti.REST_CMD_OFF:
//						cs.cambioStatoSensore(cmd);
//						//ctx.json( "{ \"stato\":" + cs.getParamSensore().stato +" }" );
//						ctx.json( cs.getParamSensore().stato );
//						break;
//
//					// latutidine = y
//					case Costanti.REST_CMD_LAT:
//					case Costanti.REST_CMD_LATITUDE:
//					case Costanti.REST_CMD_LATITUDINE:
//					case Costanti.REST_CMD_Y:
//						//cmd.param.lat =
//						cs.cambioPosizione(cmd);
//
//						ctx.json( cs.getParamSensore().lat );
//						break;
//
//					// latutidine = y
//					case Costanti.REST_CMD_LNG:
//					case Costanti.REST_CMD_LONGITUDE:
//					case Costanti.REST_CMD_LONGITUDINE:
//					case Costanti.REST_CMD_X:
//						cs.cambioStatoSensore(cmd);
//						ctx.json( cs.getParamSensore().lng );
//						break;
//
//
//					default:
//						//TODO : gestire le risposte di che non esiste e gli errori
//						ctx.result("errore");
//						break;
//				}
//			}
//
//			/*HashMap<String, String> map= new HashMap<String, String>();
//			map.put( "z" , z);
//			map.put( "s" , s);
//			map.put( "c" , c);
//			ctx.json( map );*/
//
//		});

		/*
		 * chiedo di attivare o disattivare il sitema di log
		 * /al/s123/log/on
		 * /al/s123/log/off
		 */
		server.put( "/:zona/:sens/log/:cmd", (ctx) -> {
			int nRighe = 0;
			String s = ctx.pathParam("sens");
			String c = ctx.pathParam("cmd");

			ConnessioneSensore cs = mapSensori.getOrDefault( s, null);

			try {
				nRighe = Integer.parseInt(c);
				c= Costanti.REST_CMD_NRIGHE;
			} catch ( NumberFormatException e) {

			}

			if( cs != null ) {
				switch ( c.toLowerCase() ) {

					case Costanti.REST_CMD_ON:
						cs.attivaLog();
						ctx.result( Costanti.REST_CMD_ON );
						break;

					case Costanti.REST_CMD_OFF:
						cs.disattivaLog();
						ctx.result( Costanti.REST_CMD_OFF );
						break;

					case Costanti.REST_CMD_NRIGHE:
						String contenuto = OperazioniSuFile.leggiLog(s, nRighe * 2);
						ctx.result( contenuto );
						break;

					default:
						ctx.status(400);
						break;
				}
			}

		});
		/*
		 *	chiedo la lista delle ultime righe di log
		 * /al/s123/log/numero_righe
		 */
		server.get( "/:zona/:sens/log/:cmd", (ctx) -> {
			int nRighe = 0;
			String s = ctx.pathParam("sens");
			String c = ctx.pathParam("cmd");

			ConnessioneSensore cs = mapSensori.getOrDefault( s, null);

			try { nRighe = Integer.parseInt(c); }
			catch ( NumberFormatException e) { cs = null; }

			if( cs != null ) {
				String contenuto = OperazioniSuFile.leggiLog(s, nRighe * 2);
				ctx.result( contenuto );
			}
			else { ctx.status(400); }

		});



		/*
		 * variante per inserire i valori singoli nel sistema
		 *
		 * /milano/s1478/lat/45.1235
		 * risposta: il nuovo valore
		 *
		 */
		server.put( "/:zona/:sens/:cmd/:val", (ctx) -> {

			double d = 0.0;
			String s = ctx.pathParam("sens");
			String c = ctx.pathParam("cmd");
			String v = ctx.pathParam("val");
			ConnessioneSensore cs = mapSensori.getOrDefault( s, null);
			Messaggio cmd;

			if( cs != null ) {

				cmd = new Messaggio();
				cmd.param = (CfgSensore)cs.getParamSensore();


				switch ( c.toLowerCase() ) {

					case Costanti.REST_CMD_STATO:
						cmd.cmd = v.toLowerCase();
						cs.cambioStatoSensore(cmd);
						ctx.json(cs.getParamSensore().stato );
						break;

					// se e' uno di questi ..
					// latutidine = y
					case Costanti.REST_CMD_LAT:
					case Costanti.REST_CMD_LATITUDE:
					case Costanti.REST_CMD_LATITUDINE:
					case Costanti.REST_CMD_Y:
						try {
							d = Double.parseDouble( v );
							cmd.param.lat = d;
							cs.cambioPosizione(cmd);
							ctx.json( cs.getParamSensore().lat );
						} catch( NumberFormatException ex ) { ctx.status(400); }
						break;

					// latutidine = x
					case Costanti.REST_CMD_LNG:
					case Costanti.REST_CMD_LONGITUDE:
					case Costanti.REST_CMD_LONGITUDINE:
					case Costanti.REST_CMD_X:
						try {
							d = Double.parseDouble( v );
							cmd.param.lng = d;
							cs.cambioPosizione(cmd);
							ctx.json( cs.getParamSensore().lng );
						} catch( NumberFormatException ex ) { ctx.status(400); }
						break;

					default:
						ctx.status(400);
						break;
				}
			}

		});

		/*
		 * chiedo ad un sensore uno specifico valore
		 * /al/s12345/lat
		 * risposta : 45.2222
		 *
		 * /al/s12345/stato
		 * risposta : on
		 */
		server.get( "/:zona/:sens/:cmd", (ctx) -> {

			//String z = ctx.pathParam("zona");
			String s = ctx.pathParam("sens");
			String c = ctx.pathParam("cmd");

			ConnessioneSensore cs = mapSensori.getOrDefault(s, null);
			Messaggio cmd = new Messaggio();
			cmd.cmd = c;

			if (cs != null) {
				switch (c.toLowerCase()) {

					// attiva / addormenta
					case Costanti.REST_CMD_STATO:
						ctx.json( cs.getParamSensore().stato );
						break;

					// latutidine = y
					case Costanti.REST_CMD_LAT:
					case Costanti.REST_CMD_LATITUDE:
					case Costanti.REST_CMD_LATITUDINE:
					case Costanti.REST_CMD_Y:
						ctx.json( cs.getParamSensore().lat );
						break;

					// latutidine = x
					case Costanti.REST_CMD_LNG:
					case Costanti.REST_CMD_LONGITUDE:
					case Costanti.REST_CMD_LONGITUDINE:
					case Costanti.REST_CMD_X:
						ctx.json( cs.getParamSensore().lng );
						break;

					default:
						//TODO : gestire le risposte di che non esiste e gli errori
						ctx.status(400);
						//ctx.result("errore");
						break;
				}
			}
		});