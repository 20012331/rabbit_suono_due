package restServer;


import com.google.gson.Gson;
import contenitori.CfgSensore;
import contenitori.Messaggio;

import java.util.Map;

public class PopolaMappaSensori implements Runnable {
    private Map<String , CfgSensore> mappa ;
    private String strMsg;
    private Gson gson = new Gson();

    public PopolaMappaSensori(Map<String , CfgSensore> mappa , String strMsg) {
        this.mappa = mappa;
        this.strMsg = strMsg;
    }


    @Override public void run() {
        Messaggio tmp = gson.fromJson( strMsg , Messaggio.class );
        if( tmp.cmd.equalsIgnoreCase( Messaggio.CMD_STATO ) ){
            mappa.put( tmp.nSensore , tmp.cfgSensore );
        }

    }
}
