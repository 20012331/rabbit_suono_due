package restServer;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.firebase.cloud.FirestoreClient;
import com.google.gson.Gson;
import com.rabbitmq.client.*;
import contenitori.CfgSensore;
import contenitori.Messaggio;
import contenitori.PuntoEvento;
import contenitori.RispostaOperatore;
import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.http.staticfiles.Location;
import io.javalin.websocket.*;
import optSuFile.OperazioniSuFile;

import org.eclipse.jetty.server.*;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.jetbrains.annotations.NotNull;
import partenza.Costanti;
import sens_factory.CnnManager;
import sensore.CnnBase;

import javax.annotation.Nullable;


public class RestServer {
	//private static final String PATH_KEYSTORE = "/media/universita/HD_LUCA/nedejs_prove/myapp_v2/cert/certKeyServer_pippo.ksr";
	private static final String PATH_KEYSTORE = "certificato/certKeyServer_pippo.ksr";

	private static final String PRE = Costanti.REST_LINK;
	private static final Executor esecutore = Executors.newFixedThreadPool(2);
	private static Firestore db = FirestoreClient.getFirestore();

	private Javalin server;
	private Gson gson;
	private Logger logger;
	private FileHandler fh;

	private Connection cnn = null;
	private Channel ch;
	private String codaSpedizioni = null;
	private String codaSuoni = null;


	/** per evadere le richieste del rest server */
	HashMap< String , LinkedList<Context> > mapEvasione = new HashMap<>();
	/** accumumulo dei sensori */
	HashMap< String , CfgSensore> mapSensori = new HashMap<>();

	private LinkedList< WsContext > listaClient = new LinkedList<>();

	// lo hai fatto, tesoro ?!?!?!?
	// TODO : VARIATO, ad intervalli di 15 secondi il sensore si annuncia da solo.
	//  <== fare in modo che appena connesso il rest server chieda lo stato a tutti i sensori in ascolto
	// TODO : FATTO, https e wss anche rabbit
	//  <== abilitare ssl
	// TODO : verificare i suono con il cambio citta e citta diverse
	// TODO : abilitare la possibilita di spegnere il server
	// TODO : rendere il server indipendente
	// TODO : aggiungere un comando in manager per riattivare i sensori spenti


	public RestServer() {
		gson = new Gson();
		impostaFileHandler( this.getClass().getName() );

	}


	public void attiva(){
		if( creaConnessione() ){
			creaCanale();
			start();
			logger.info( "rest server su porta : " + server.port());
		}
	}


	public void start() {

		initFirestore();
		
		server = Javalin.create( jConfig -> {
			jConfig.addStaticFiles( "/media/universita/HD_LUCA/nedejs_prove/myapp_v2/public/", Location.EXTERNAL);

			jConfig.server( () -> {
				// qui sistemo HTTPS
				org.eclipse.jetty.util.ssl.SslContextFactory.Server sslCtxServer = new SslContextFactory.Server.Server();
				sslCtxServer.setKeyStorePath( PATH_KEYSTORE );
				sslCtxServer.setKeyStorePassword( "pippopippo");

				//SslConnectionFactory ssCnnFact = new SslConnectionFactory( sslCtxServer , "https" );

				Server sslServer = new Server();
				ServerConnector sslConnector = new ServerConnector( sslServer, sslCtxServer ) ;
				sslConnector.setPort(13443);
				ServerConnector connector = new ServerConnector( sslServer );
				connector.setPort( 13080 );
				sslServer.setConnectors(new Connector[]{sslConnector, connector});

				return sslServer;
			});
		});

		server.start( Costanti.REST_PORT );
		server.ws("/" , wsHandler -> {
			wsHandler.onConnect( wsConnesso);
			wsHandler.onClose( wsDisconnesso );
			wsHandler.onMessage( wsMessaggio );
			//wsHandler.onError( wsNonso );
		});


		server.get( PRE + "/:sens" , ctx -> {
			String sens = ctx.pathParam("sens");
			CfgSensore tmpCfg = mapSensori.getOrDefault( sens , null );

			if ( sens.equalsIgnoreCase(Costanti.REST_SENSORI)) {
				LinkedList<CfgSensore> lista = new LinkedList<>();
				mapSensori.forEach((s, cfgSensore) -> {
					lista.add(cfgSensore);
				});
				ctx.json(lista);
			} else if ( sens.equalsIgnoreCase( Costanti.REST_HELP ) ) {
				ctx.result( Costanti.TXT_HELP );
			} else if ( tmpCfg != null ) {
				ctx.json( tmpCfg );
			} else {
				ctx.status( 404 );
			}
		});
		server.get( PRE + "/:citta/:sens", (ctx) -> {

			String c = ctx.pathParam("citta");
			String s = ctx.pathParam("sens");
			CfgSensore tmp = mapSensori.getOrDefault( s , null );

			// se voglio tutti i sensori di una certa citta
			if( s.equalsIgnoreCase( Costanti.REST_SENSORI ) ) {

				LinkedList< CfgSensore > lista = new LinkedList<>();
				mapSensori.forEach( ( nSens, cfgSensore ) -> {
					if( cfgSensore.citta.equalsIgnoreCase( c ) ){
						lista.add( cfgSensore );
					}
				});
				ctx.json( lista );

				// se un sensore specifico
			} else if( tmp != null ){
				if( tmp.citta.equalsIgnoreCase( c ) ){

					ctx.json( tmp );
				}
			}

		});
		server.put( PRE + "/:citta/:sens", (ctx) -> {

			String c = ctx.pathParam("citta");
			String s = ctx.pathParam("sens");

			//msg.cmd = Messaggio.CDM_NEW_CFG;
			CfgSensore cfgHtml = ctx.bodyAsClass( CfgSensore.class );
			CfgSensore cfgOrig = mapSensori.getOrDefault( s , null );

			Messaggio msg = new Messaggio();

			if( (cfgHtml != null) && ( cfgOrig != null ) && ( cfgOrig.citta.equalsIgnoreCase( c ) ) ){

				putJsonStato( cfgHtml , cfgOrig , c , s);
				putJsonLog( cfgHtml , cfgOrig , c , s);
				putJsonLL( cfgHtml , cfgOrig , c , s);
				//putJsonCitta(cfgHtml , cfgOrig , c , s);







			} else { ctx.status( 404 ); }

		});
		server.get( PRE + "/:citta/:sens/:azione", (ctx) -> {

			String c = ctx.pathParam("citta");
			String s = ctx.pathParam("sens");
			String act = ctx.pathParam("azione");

			CfgSensore tmp = mapSensori.getOrDefault( s , null );
			if( tmp != null ){
				if( tmp.citta.equalsIgnoreCase( c ) ){

					switch ( act.toLowerCase() ){
						case Costanti.REST_CMD_STATO :
							ctx.json( tmp.stato );
							break;

						case Costanti.REST_CMD_LAT:
						case Costanti.REST_CMD_LATITUDE:
						case Costanti.REST_CMD_LATITUDINE:
						case Costanti.REST_CMD_Y:
							ctx.json( tmp.lat );
							break;

						case Costanti.REST_CMD_LNG:
						case Costanti.REST_CMD_LONGITUDE:
						case Costanti.REST_CMD_LONGITUDINE:
						case Costanti.REST_CMD_X:
							ctx.json( tmp.lng );
							break;

						case Costanti.REST_CMD_POS:
						case Costanti.REST_CMD_POSIZIONE:
							HashMap<String , Double > hm = new HashMap<>();
							hm.put( "lat" , tmp.lat );
							hm.put( "lng" , tmp.lng );
							ctx.json( hm );
							break;

						case Costanti.REST_CMD_LOG:
							ctx.json( tmp.log );
							break;

						case Costanti.REST_CMD_CITTA:
							ctx.json( tmp.citta );
							break;
							
						default:
							ctx.status( 404 );
							break;
					}


				} else { ctx.status( 404 ); }

			} else { ctx.status( 404 ); }
		});
		server.put( PRE + "/:zona/:sens/:cmd/:val", (ctx) -> {
			String zona = ctx.pathParam("zona");
			String sens = ctx.pathParam("sens");
			String cmd = ctx.pathParam("cmd");
			String val = ctx.pathParam("val");

			boolean spedisco = false;
			double d ;

			CfgSensore tmp = mapSensori.getOrDefault( sens , null );
			Messaggio msg = new Messaggio();

			if( tmp != null ){
				if( tmp.citta.equalsIgnoreCase( zona ) ){

					// preparo il messaggio da spedire
					msg.cfgSensore = Costanti.deepCopy( tmp );
					msg.nSensore = tmp.nSensore;

					switch ( cmd.toLowerCase() ){

						case Costanti.REST_CMD_STATO :
							if( val.equalsIgnoreCase( CfgSensore.PAR_STATO_ATTIVO ) ||
								val.equalsIgnoreCase( CfgSensore.PAR_STATO_PASSIVO) ){
								msg.cmd = val.toLowerCase();
								spedisco = true;
							}
							break;

						case Costanti.REST_CMD_LOG:
							if( val.equalsIgnoreCase( CfgSensore.PAR_STATO_ATTIVO ) ){
								msg.cmd = Messaggio.CMD_LOG_ON;
								spedisco = true;
							} else if ( val.equalsIgnoreCase( CfgSensore.PAR_STATO_PASSIVO) ) {
								msg.cmd = Messaggio.CMD_LOG_OFF;
								spedisco = true;
							}
							break;

						case Costanti.REST_CMD_LAT:
						case Costanti.REST_CMD_LATITUDE:
						case Costanti.REST_CMD_LATITUDINE:
						case Costanti.REST_CMD_Y:
							msg.cmd = Messaggio.CMD_NEW_POS;
							d = convertiLL( val , msg.cfgSensore.lat , 90 );
							msg.cfgSensore.lat = d;
							spedisco = true;
							break;

						case Costanti.REST_CMD_LNG:
						case Costanti.REST_CMD_LONGITUDE:
						case Costanti.REST_CMD_LONGITUDINE:
						case Costanti.REST_CMD_X:
							msg.cmd = Messaggio.CMD_NEW_POS;
							d = convertiLL( val , msg.cfgSensore.lng , 180 );
							msg.cfgSensore.lng = d;
							spedisco = true;
							break;

						case Costanti.REST_CMD_CITTA:
							msg.cmd = Messaggio.CMD_CAMBIO_CITTA;
							msg.cfgSensore.nSensore = val;
							spedisco = true;
							break;

						default:
							ctx.status( 404 );
							break;
					}

				}

			}

			if( spedisco ){
				sendCmdRicevuto( gson.toJson( msg ), msg );
				//ctx.status( 200 );
				//int fine = ctx.url().lastIndexOf('/');
				//String subUrl = ctx.url().substring( 0 , fine);
				//ctx.redirect( subUrl );
			} else { ctx.status( 404 ); }

		});

	}

	private void putJsonCitta(CfgSensore cfgHtml, CfgSensore cfgOrig, String citta, String sens) {
		Messaggio msg = new Messaggio();
		msg.cfgSensore = Costanti.deepCopy( cfgOrig );
		msg.nSensore = sens;

		//FIXME : fare in modo che la citta non cambi in caso di
		if( (cfgHtml.citta.equalsIgnoreCase( citta ) == false) && (cfgHtml.citta.trim().length() > 1 ) ){
			msg.cmd = Messaggio.CMD_CAMBIO_CITTA;
			// devo cambiare ,altrimenti non so la coda originale a cui spedire
			// e il muovo nome che devo usare
			msg.cfgSensore.nSensore = cfgHtml.citta;
			msg.cfgSensore.citta = cfgOrig.citta;
			sendCmdRicevuto( gson.toJson( msg ) , msg );
		}
	}

	private void putJsonLL(CfgSensore cfgHtml, CfgSensore cfgOrig, String citta, String sens) {
		boolean spedPos = false;

		Messaggio msg = new Messaggio();
		msg.cfgSensore = Costanti.deepCopy( cfgOrig );
		msg.nSensore = sens;
		// modifica posizione
		if( ( cfgHtml.lat != cfgOrig.lat ) && ( cfgHtml.lat !=0.0 ) ) {
			msg.cfgSensore.lat = cfgHtml.lat;
			spedPos = true;
		} else { cfgHtml.lat = cfgOrig.lat; }

		if( ( cfgHtml.lng != cfgOrig.lng ) && ( cfgHtml.lng !=0.0 ) ) {
			msg.cfgSensore.lng = cfgHtml.lng;
			spedPos = true;

		} else { cfgHtml.lng = cfgOrig.lng; }

		if( spedPos ){
			msg.cmd = Messaggio.CMD_NEW_POS;
			sendCmdRicevuto( gson.toJson( msg ) , msg );
		}
	}
	private void putJsonLog(CfgSensore cfgHtml, CfgSensore cfgOrig, String citta, String sens) {
		Messaggio msg = new Messaggio();
		msg.cfgSensore = Costanti.deepCopy( cfgOrig );
		msg.nSensore = sens;
		// cambio log
		if( (cfgHtml.log != null) && (false == cfgHtml.log.equalsIgnoreCase( cfgOrig.log ) ) ){

			if( cfgHtml.log.equalsIgnoreCase( CfgSensore.PAR_STATO_ATTIVO ) ){
				msg.cmd = Messaggio.CMD_LOG_ON;
				msg.cfgSensore.log = CfgSensore.PAR_STATO_ATTIVO;
				sendCmdRicevuto( gson.toJson( msg ) , msg );

			} else if( cfgHtml.log.equalsIgnoreCase( CfgSensore.PAR_STATO_PASSIVO ) ){
				msg.cmd = Messaggio.CMD_LOG_OFF;
				msg.cfgSensore.log = CfgSensore.PAR_STATO_PASSIVO;
				sendCmdRicevuto( gson.toJson( msg ) , msg );

			}

		}
	}

	private void putJsonStato(CfgSensore cfgHtml, CfgSensore cfgOrig, String citta , String sens) {
		Messaggio msg = new Messaggio();
		msg.cfgSensore = Costanti.deepCopy( cfgOrig );
		msg.nSensore = sens;
		//msg.cfgSensore.citta = c;

		// cambio stato stato
		// se il valore e' corretto e diverso dallo originale, allora spedisci il cambio
		if( (cfgHtml.stato != null) && false == cfgHtml.stato.equalsIgnoreCase( cfgOrig.stato ) ){

			if(cfgHtml.stato.equalsIgnoreCase( CfgSensore.PAR_STATO_ATTIVO) ){
				msg.cmd = CfgSensore.PAR_STATO_ATTIVO;
				sendCmdRicevuto( gson.toJson( msg ) , msg );

			} else if ( cfgHtml.stato.equalsIgnoreCase( CfgSensore.PAR_STATO_PASSIVO) ) {
				msg.cmd = CfgSensore.PAR_STATO_PASSIVO;
				sendCmdRicevuto( gson.toJson( msg ) , msg );
			}

		}
	}






	private double convertiLL( String numero , double originale , int limite){
		double d = originale;
		try{
			d = Double.parseDouble( numero );
			if( ( d > limite) || ( d < -limite) ){
				d = originale;
			}
		} catch ( NumberFormatException ex){
			logger.info( numero + " : non e un numero valido");
		}
		return d;
	}







//					switch ( act.toLowerCase() ){
//		case Costanti.REST_CMD_STATO :
//			break;
//		case Costanti.REST_CMD_LAT:
//		case Costanti.REST_CMD_LATITUDE:
//		case Costanti.REST_CMD_LATITUDINE:
//		case Costanti.REST_CMD_Y:
//			break;
//		case Costanti.REST_CMD_LNG:
//		case Costanti.REST_CMD_LONGITUDE:
//		case Costanti.REST_CMD_LONGITUDINE:
//		case Costanti.REST_CMD_X:
//			break;
//		case Costanti.REST_CMD_POS:
//		case Costanti.REST_CMD_POSIZIONE:
//			break;
//	}





















	/* *****************************************************************************************************************
	 * impostazioni logger
	 **************************************************************************************************************** */
	/**
	 * per il log su file
	 * @param nomeFile_e_Logger
	 */
	private void impostaFileHandler( String nomeFile_e_Logger ) {
		if( fh == null ) {

			try {
				fh = new FileHandler("logs/" + nomeFile_e_Logger , false);
				fh.setFormatter( new SimpleFormatter() );

				logger = Logger.getLogger( nomeFile_e_Logger );
				logger.addHandler( fh );

			} catch (SecurityException | IOException e) { e.printStackTrace();}
		}
	}

	public void disattivaLog() 	{ logger.setLevel(Level.OFF); }
	public void attivaLog() 	{ logger.setLevel(Level.ALL); }















	/* *****************************************************************************************************************
	 * Connessione a rabbit
	 **************************************************************************************************************** */
	private boolean creaConnessione(){
		CnnBase cBase = new CnnBase(
				OperazioniSuFile.leggiFileConnessione()
		);
		cnn = cBase.getConnessione();
		return ( null != cnn );
	}
	/**
	 * Si connette a Rabbit con i dati precedentemente impostati
	 */
	private void creaCanale(){

		try {
			ch = cnn.createChannel();
			initRiceviCmd();
			initRiceviSnd();
			// tutti i messaggi che arrivano a questa coda, vengono gestiti
			ch.basicConsume( codaSpedizioni , true, cbRicevoCmd , (cancelCb)-> {} );

		} catch (IOException ex) {
			Logger.getLogger( CnnManager.class.getName() ).log(Level.SEVERE, null, ex);
		}
	}
	private void initRiceviCmd() throws IOException {
		ch.exchangeDeclare(Costanti.DEF_EX_CMD , BuiltinExchangeType.TOPIC );
		codaSpedizioni = ch.queueDeclare().getQueue();
		ch.queueBind( codaSpedizioni, Costanti.DEF_EX_CMD, Costanti.DEF_EX_SPD );
	}
	private void initRiceviSnd() throws IOException {
		ch.exchangeDeclare(Costanti.DEF_EX_SND , BuiltinExchangeType.TOPIC );
		//codaSuoni = ch.queueDeclare().getQueue();
		//ch.queueBind( codaSuoni, Costanti.DEF_EX_CMD, Costanti.DEF_EX_SPD );
	}
	// cosa devo fare quando arriva un messaggio ad una certa coda
	private DeliverCallback cbRicevoCmd = new DeliverCallback() {

		@Override
		public void handle(String consumaTag, @NotNull Delivery ricevuto) throws IOException {

			String msgRicevuto = new String( ricevuto.getBody() );
			//Messaggio msg = gson.fromJson( msgRicevuto, Messaggio.class );
			spedisciTuttiWs( msgRicevuto);
			updateMappaSensori( msgRicevuto );
		}
	};

	private void updateMappaSensori( String strMsg ){
		esecutore.execute( new PopolaMappaSensori( mapSensori , strMsg ) );
	}

















	/* *****************************************************************************************************************
	 * cll back e funzioni ws
	 **************************************************************************************************************** */


	/**
	 * spedisco in broadcast
	 * @param msgJson un messaggio in formato stringa
	 */
	private void spedisciTuttiWs( String msgJson ){
		listaClient.forEach( ctx -> {
			ctx.send( msgJson );
		});
	}

	/**
	 * quando arriva qualcosa le smisto dove serve
	 * @param messaggio in formato stringa
	 */
	private void gestioneMessaggioRicevuto( String messaggio){
		Messaggio msg = gson.fromJson( messaggio , Messaggio.class );
		switch ( msg.cmd.toLowerCase() ){
			case Messaggio.CMD_INIT_EVENTO_SUONO:
				recuperaPrimi10();
				break;
			case Messaggio.CMD_SUONO:
				sendSuonoRicevuto( messaggio , msg);
				logger.info( messaggio );
				break;
			default:
				sendCmdRicevuto( messaggio , msg);
				logger.info( messaggio );
				break;
		}

	}

	private void sendSuonoRicevuto( String messaggio , Messaggio tmp){
		try {
			ch.basicPublish(
					tmp.cfgSensore.exCmd ,
					tmp.cfgSensore.citta , null ,
					messaggio.getBytes()
			);
			/*ch.basicPublish(
					Costanti.DEF_EX_SND ,
					Costanti.DEF_RK_SND , null ,
					messaggio.getBytes()
			);*/
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void sendCmdRicevuto( String messaggio , Messaggio tmp ){
		try {
			ch.basicPublish(
					tmp.cfgSensore.exCmd ,
					tmp.cfgSensore.citta , null ,
					messaggio.getBytes()
			);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private WsConnectHandler wsConnesso = new WsConnectHandler() {
		@Override
		public void handleConnect(@NotNull WsConnectContext ctx) {
			ctx.attributeMap().forEach( (s, o) -> {
				System.out.println( s );
			});
			listaClient.add( ctx );
		}
	};
	private WsCloseHandler wsDisconnesso = new WsCloseHandler() {
		@Override
		public void handleClose(@NotNull WsCloseContext ctx) {
			listaClient.remove( ctx );
		}
	};
	private WsMessageHandler wsMessaggio = new WsMessageHandler() {
		@Override
		public void handleMessage(@NotNull WsMessageContext ctx) {
			gestioneMessaggioRicevuto( ctx.message() );
		}
	};















































	/* *****************************************************************************************************************
	 * firestore funzioni
	 **************************************************************************************************************** */
	public static final String CMD_EVENTO_SUONO = "evento_suono";
	public static final String CMD_EVENTO_OP = "rispOp";
	public static final String FB_DOC_EVENTO_PE = "puntoEvento";
	public static final String  FB_DOC_EVENTO_RO = "rispostaOperatore";
	public static final String CAMPO_ID = "id";
	public static final int MAX_RIGHE_QUERY = 10;
	public static final String CAMPO_RO_ORA_RISP = "oraRisposta";

	//TODO: chiudere i listener
	private ListenerRegistration rlPE;
	private ListenerRegistration rlRo;

	private void initFirestore() {
		// ascolto eventi
		rlPE = db.collection( FB_DOC_EVENTO_PE ).addSnapshotListener(listenerRispPE);
		rlRo = db.collection( FB_DOC_EVENTO_RO ).addSnapshotListener(listenerRispOP);
	}

	private EventListener<QuerySnapshot> listenerRispPE = new EventListener<QuerySnapshot>() {
		@Override
		public void onEvent(@Nullable QuerySnapshot value, @Nullable FirestoreException error) {
			if( value != null ){ ciclaNuoviPE( value ); }
		}
	};
	private EventListener<QuerySnapshot> listenerRispOP = new EventListener<QuerySnapshot>() {
		@Override
		public void onEvent(@Nullable QuerySnapshot value, @Nullable FirestoreException error) {
			if( value != null ){ ciclaNuoviRO( value ); }
		}
	};

	private void ciclaNuoviPE(QuerySnapshot value) {
		for ( QueryDocumentSnapshot doc : value.getDocuments() ){

			Messaggio msg = new Messaggio();
			msg.cmd = CMD_EVENTO_SUONO;
			//msg.cfgSensore = gson.toJson(  );
			msg.puntoEvento = doc.toObject( PuntoEvento.class );
			spedisciTuttiWs( gson.toJson( msg ) );

		}
	}
	private void ciclaNuoviRO(QuerySnapshot value) {
		for ( QueryDocumentSnapshot doc : value.getDocuments() ){

			Messaggio msg = new Messaggio();
			msg.cmd = CMD_EVENTO_OP;
			msg.rispostaOperatore = doc.toObject( RispostaOperatore.class );
			spedisciTuttiWs( gson.toJson( msg ) );

		}
	}

	private void recuperaPrimi10(){
		esecutore.execute(
			new RunPE(
				db.collection( FB_DOC_EVENTO_PE )
					.orderBy( CAMPO_ID , Query.Direction.DESCENDING )
					.limit( MAX_RIGHE_QUERY ).get()
			)
		);
		esecutore.execute(
			new RunRO(
				db.collection( FB_DOC_EVENTO_RO )
					.orderBy( CAMPO_RO_ORA_RISP , Query.Direction.DESCENDING )
					.limit( (MAX_RIGHE_QUERY * 2) )
					.get()
			)
		);
	}
	class RunPE implements  Runnable {
		private ApiFuture< QuerySnapshot > fut;
		public RunPE(ApiFuture< QuerySnapshot > fut) {
			this.fut = fut;
		}
		@Override public void run() {
			try {
				ciclaNuoviPE( fut.get() );
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
	}
	class RunRO implements  Runnable {
		private ApiFuture< QuerySnapshot > fut;
		public RunRO(ApiFuture< QuerySnapshot > fut) {
			this.fut = fut;
		}
		@Override public void run() {
			try {
				ciclaNuoviRO(fut.get() );
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
	}

}
