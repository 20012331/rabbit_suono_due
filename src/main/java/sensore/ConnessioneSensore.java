/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensore;

import com.google.cloud.firestore.Firestore;
import com.google.firebase.cloud.FirestoreClient;
import com.google.gson.Gson;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Delivery;

import contenitori.*;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import optSuFile.OperazioniSuFile;
import org.jetbrains.annotations.NotNull;
import partenza.Costanti;


/**
 *
 * @author universita
 */
public class ConnessioneSensore {
    
    private static Firestore db = FirestoreClient.getFirestore();
    private Logger logger;
    //private static Logger annota = LogManager.getLogger();
    private FileHandler fh;

    private ScheduledExecutorService servizietto = Executors.newSingleThreadScheduledExecutor();

    private Connection cnn = null;
    private CfgConnessione cfgOpt = null;
    private CfgSensore cfgSensore = null;
    private Channel ch = null;

    // nomi delle code in cui sono registrato
    private String codaComandi;
    //private String codaSuoni;
    //private String codaSpedizioni;
    
    private Sensore sensore; 
    private Gson gson;

    private Runnable annuncio = new Runnable() {
        @Override public void run() {
            try {
                if( ch != null ) {
                    richiestaStatoSensore();
                }
            } catch (IOException ex) {
                logger.log( Level.SEVERE , null , ex );
                logger.severe( "errore annuncio periodico" );
            }
        }
    };

    public ConnessioneSensore( String nomeSensore ){
        gson = new Gson();
        impostaFileHandler( nomeSensore );

        cfgOpt = OperazioniSuFile.leggiFileConnessione();
        cfgSensore = OperazioniSuFile.leggiFileSensore( nomeSensore );
        setLogOnOff();
    }

    private boolean creaConnessione(){

        CnnBase cBase = new CnnBase( cfgOpt );
        cnn = cBase.getConnessione();
        // se tutto va bene, preparo il sensore
        if( cnn != null ){ sensore = new Sensore( cfgSensore ); }
        return ( null != cnn );

    }
    private void creaCanale(){
        try {
            // connetto con tutti i parametri di configurazione
            // se tutto e andato bene, posso aprire il canale di comunicazione
            ch = cnn.createChannel();

            initRiceviCmd();
            //initRiceviSuoni();
            //initSpedizioni();

            // tutti i messaggi che arrivano a questa coda, vengono gestiti
            //ch.basicConsume( codaComandi , true, cbRicevoCmd , (cancelCb)-> {} );
            //ch.basicConsume( codaSuoni , true, cbRicevoSuono , (cancelCb)-> {} );

            // avviso tutti che sono arrivato
            spedisciMsg( generaMsgSensore() );

        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    /**
     * il messaggio con oggetto la sua configurazione
     * @return un messaggio con lo stato del sensore
     */
    public Messaggio generaMsgSensore(){ return generaMsgSensore( Messaggio.CMD_STATO ); }
    /**
     * il messaggio con oggetto la sua configurazione
     * @param cmd comando da mandare assieme al messaggio
     * @return
     */
    public Messaggio generaMsgSensore(String cmd ){
        Messaggio msg = new Messaggio();
        msg.nSensore = sensore.getNomeSensore();
        msg.cmd = cmd;
        msg.cfgSensore = sensore.getCfgSensore();
        msg.cfgSensore.log = cfgSensore.log;
        msg.cfgSensore.citta = cfgSensore.citta;
        msg.cfgSensore.exCmd = cfgSensore.exCmd;
        msg.cfgSensore.rkSpd = cfgSensore.rkSpd;
        return msg;
    }

    /**
     * Si connette a Rabbit con i dati precedentemente impostati
     */
    public void attiva(){
        if( creaConnessione() ){
            creaCanale();
            servizietto.scheduleWithFixedDelay( annuncio , 5 , CfgSensore.TEMPO_ANNUNCIO , TimeUnit.SECONDS );
        } else {
            logger.severe( this.getClass().getName() + " : chiuso per problemi di connessione" );
        }
    }

    public void updateCanale(){
        // chiudo il canale e lo reinizializzo con i nuovi valori che sono arrivati
        try { ch.abort(); } catch (IOException ex) { logger.log(Level.SEVERE, null, ex); }
        creaCanale();
    }
    
    
    /** chiamarlo per scrivere su file le impostazioni
     * e chiude il canale
     */
    public void disattiva() {

        // prendo glli ultimi dati, quelli del canale sono gia aggiornati
        //cfgSensore = sensore.getCfgSensore();

    	try {
    	    // spengo l'annuncio
            servizietto.shutdownNow();

            // creo un messaggio di avviso di disconnessione
            // spedisco il messaggio
    		spedisciMsg( generaMsgSensore( Messaggio.CMD_QUIT ) );

    		// scrivo su file / aggiorno i valori
            //OperazioniSuFile.scriviCfgSensore( sensore.getNomeSensore() , gson.toJson( cfgFile ) );
            aggiornaFileCfg();

            // chiudo canale e connessione , anche se bastava il secondo
            ch.close();
            cnn.abort();



        } catch (IOException | TimeoutException ex) {

            logger.log(Level.SEVERE, null, ex);
            logger.log(Level.SEVERE , " Errore chiusura canale" );

        }
    }
    private void aggiornaFileCfg(){
        //cfgSensore = sensore.getCfgSensore();
        // aggiorno file
        OperazioniSuFile.scriviCfgSensore( sensore.getNomeSensore(), gson.toJson( cfgSensore ) );
    }
    private void initRiceviCmd() throws IOException {
        // idempotente
        // definisco il tipo di exchange = topic
        ch.exchangeDeclare( cfgSensore.exCmd, BuiltinExchangeType.TOPIC );
        
        // definsco una coda
        // ma la lascio fecifere al server 
        // la cida dichuarata in queso moda e:
        // non persistente      = se cade il server perdo tutto
        // esclusiva            = valida solo per la sessione in corso
        // si cancella da sola  = quando non piu in uso
        codaComandi = ch.queueDeclare().getQueue();
        
        // associo la coda allo exchange
        // e dichiaro a quali topic ( pattern di topic) sono interessato
        ch.queueBind( codaComandi, cfgSensore.exCmd, cfgSensore.citta );
        ch.queueBind( codaComandi, cfgSensore.exCmd, Costanti.DEF_ITALIA );

        // tutti i messaggi che arrivano a questa coda, vengono gestiti
        ch.basicConsume( codaComandi , true, cbRicevoCmd , (cancelCb)-> {} );
    }
    
    /*private void initRiceviSuoni() throws IOException {
    	ch.exchangeDeclare( cfgFile.cfgChannel.nome_exchange_suoni , BuiltinExchangeType.TOPIC );
    	codaSuoni = ch.queueDeclare().getQueue();
    	ch.queueBind( codaSuoni , cfgFile.cfgChannel.nome_exchange_suoni , cfgFile.cfgChannel.routng_key_suoni );
        ch.basicConsume( codaSuoni , true, cbRicevoSuono , (cancelCb)-> {} );
    }*/
    /*private void initSpedizioni() throws IOException {
    	ch.exchangeDeclare( cfgOpt.NOME_EXCHANGE_SPEDIZIONE , BuiltinExchangeType.TOPIC );
    	codaSpedizioni = ch.queueDeclare().getQueue();
    	ch.queueBind( codaSpedizioni , cfgOpt.NOME_EXCHANGE_SPEDIZIONE, cfgOpt.ROUTNG_KEY_SPEDIZIONE );
    }*/


 // cosa devo fare quando arriva un messaggio ad una certa coda
    /*private DeliverCallback cbRicevoSuono = new DeliverCallback() {

        @Override
        public void handle(String consumaTag, Delivery ricevuto) throws IOException {

            if( sensore.isAttivo() ) {
                // ottengo il suono in json
                String msgRicevuto = new String( ricevuto.getBody() );
                Suono suono = gson.fromJson( msgRicevuto , Suono.class );

                // se e' destinato a me , lo elaboro
                if ( suono.nSensore.equalsIgnoreCase( sensore.getNomeSensore() ) ) {

                    EqRetta retta = sensore.ricevoSuono(suono);
                    //String rettaJson = gson.toJson(retta);


                    // se e' attivo
                    if( sensore.isAttivo() ) {
                        // mando a db cloudFirestore
                        db.collection( Costanti.FB_DOC_RETTE_SUONI ).add( retta );
                    }


                    System.out.println( gson.toJson( retta ) );
                    //System.out.println( retta.m + "* x + " + retta.q );
                }

            }
        }
    };*/
    
    
    // cosa devo fare quando arriva un messaggio ad una certa coda
    private DeliverCallback cbRicevoCmd = new DeliverCallback() {
        
        @Override
        public void handle(String consumaTag, Delivery ricevuto) throws IOException {
        	
            String msgRicevuto = new String( ricevuto.getBody() );
            Messaggio msg = gson.fromJson( msgRicevuto, Messaggio.class );

            if( msg.nSensore.equalsIgnoreCase( sensore.getNomeSensore() ) ||
                msg.nSensore.equalsIgnoreCase( Messaggio.TUTTI_SENSORI )
            ) {

                logger.info("[ ricevo ] da " + ricevuto.getEnvelope().getRoutingKey() + " : " + msgRicevuto );

                switch ( msg.cmd.toLowerCase() ) {
                    // ****************************************
                    // parte dedicata al sensore
                    // ****************************************
                    case Messaggio.CMD_STATO:
                        richiestaStatoSensore();
                        break;

                    case Messaggio.CMD_NEW_POS:
                        cambioPosizione(msg);
                        break;

                    case CfgSensore.PAR_STATO_ATTIVO:
                    case CfgSensore.PAR_STATO_PASSIVO:
                        cambioStatoSensore( msg );
                        break;

                    case Messaggio.CMD_SUONO:
                        gestisciSuono( msg );
                        break;

                    case Messaggio.CMD_LOG_ON:
                        attivaLog();
                        richiestaStatoSensore();
                        break;
                    case Messaggio.CMD_LOG_OFF:
                        disattivaLog();
                        richiestaStatoSensore();
                        break;

                    case Messaggio.CMD_LOG_LIST:
                        logDaFile( msg );
                        break;
                    //case Messaggio.CMD_NEW_NOME: nuovoNomeSensore(cmd); break;

                    // ****************************************
                    // parte dedicata al canale o alla chiusura
                    // ****************************************
                    case Messaggio.CMD_CAMBIO_CITTA:
                        cambioCitta( msg );
                        break;
                    case Messaggio.CMD_CAMBIO_EX_CMD:
                        cambioExCmd( msg );
                        break;
                    case Messaggio.CMD_CAMBIO_EX_SPED:
                        cambioExSpd( msg );
                        break;

                    case Messaggio.CMD_QUIT:
                        disattiva();
                        break;
                        
                    default:
                        break;

                }
            }
    	}            
    };



    public void richiestaStatoSensore() throws IOException{ spedisciMsg( generaMsgSensore() );  }
    /**
     * @param tmp che sia CfgSensore.PAR_STATO_ATTIVO o passivo
     * @throws IOException
     */
    public void cambioStatoSensore( Messaggio tmp ) throws IOException{ cambioStatoSensore( tmp.cmd ); }
    public void cambioStatoSensore( String stato ) throws IOException{
        // cambio stato
        sensore.setStatoSensore( stato );
        //aggiornaFileCfg();
        // comunico cambiamento
        richiestaStatoSensore();
    }

    public void cambioPosizione( String obj ) throws IOException{
        cambioPosizione( gson.fromJson( obj , CfgSensore.class ) );
    }
    public void cambioPosizione(@NotNull Messaggio msg ) throws IOException{
        cambioPosizione( msg.cfgSensore);
    }
    public void cambioPosizione(@NotNull CfgSensore tmp ) throws IOException{
        // imposto la nuova posizione
        sensore.setPosizioneMappa( tmp.lng , tmp.lat , true);

        // la comunico
        richiestaStatoSensore();

        // aggiorno il file
        aggiornaFileCfg();
        
    }

    public void gestisciSuono( String obj){
        gestisciSuono( gson.fromJson( obj , Suono.class ) );
    }
    public void gestisciSuono( Messaggio tmp ){ gestisciSuono( tmp.suono ); }
    public void gestisciSuono( Suono suono){

        if( sensore.isAttivo() ) {
            // eseguo il calcolo
            EqRetta retta = sensore.ricevoSuono(suono);

            // mando a db cloudFirestore
            db.collection( Costanti.FB_DOC_RETTE_SUONI ).add( retta );

            logger.info( "[ spedisco ] " + gson.toJson( retta ) );

        }

    }
    public void cambioCitta(@NotNull Messaggio tmp){
        cfgSensore.citta = tmp.cfgSensore.nSensore;
        updateCanale();
    }
    public void cambioExCmd(@NotNull Messaggio tmp ){
        cfgSensore.exCmd = tmp.cfgSensore.exCmd;
        updateCanale();
    }
    public void cambioExSpd(@NotNull Messaggio tmp ){
        cfgSensore.rkSpd = tmp.cfgSensore.rkSpd;
        updateCanale();
    }


    /*public void nuovoNomeSensore( Messaggio cmd ) throws IOException{
        // cancello le impostazioni
        OperazioniSuFile.cancellaFileSensore( sensore.getNomeSensore() );
        
        String nNome = cmd.param.stato;
        // comunico la morte del sensore 
        completaIlComando( cmd );
        cmd.cmd = Messaggio.CMD_QUIT;
        spedisciMsg( cmd );
                
        // rispondo come se fossi un nuovo sensore appena giunto
        sensore.setNomeSensore( nNome );
        richiestaStatoSensore(cmd);
        
        // scrivo le nuove impostazioni
        ParamCfgSensore pCfg = (ParamCfgSensore) cmd.param;
        pCfg.nSensore = cmd.nSensore;
        OperazioniSuFile.scriviSensore( sensore.getNomeSensore(), gson.toJson( pCfg ) );
        
    }*/
    
    public CfgSensore getCfgSensore() { return sensore.getCfgSensore(); }
    
    
    
    
    /**
     * spedisce 
     * @param cmd
     * @throws IOException
     */
    private void spedisciMsg(Messaggio cmd ) throws IOException {

		ch.basicPublish(
                cfgSensore.exCmd,
                cfgSensore.rkSpd,
                null,
                gson.toJson( cmd ).getBytes() );


		logger.log( Level.INFO, "[Spedisco] " + gson.toJson(cmd) );
    	
    }
    private void spedisciLog(Messaggio cmd ) throws IOException {
        logger.log( Level.INFO, "[Spedisco ] il Log" );
        ch.basicPublish(
                cfgSensore.exCmd,
                cfgSensore.rkSpd,
                null,
                gson.toJson( cmd ).getBytes() );
    }

    /*private void completaIlComando( Messaggio cmd ) {
    	cmd.cmd = Messaggio.CMD_STATO;	// questo come risposta
        cmd.nSensore = sensore.getNomeSensore();
        cmd.obj = gson.toJson( sensore.getCfgSensore() );
    }*/


    /*protected static void impostaFileHandler() {
    	if( fh == null ) {
    		
    		try {
				fh = new FileHandler("logs/loooool", true);
				fh.setFormatter( new SimpleFormatter() );
				
				logger = Logger.getLogger( "si" );
				logger.addHandler( fh );
				
			} catch (SecurityException | IOException e) { e.printStackTrace();}
    	}
    }*/

    /**
     * per il log su file
     * @param nomeFile_e_Logger
     */
    private void impostaFileHandler( String nomeFile_e_Logger ) {
    	if( fh == null ) {
    		
    		try {
				fh = new FileHandler("logs/" + nomeFile_e_Logger , false);
				fh.setFormatter( new SimpleFormatter() );
				
				logger = Logger.getLogger( nomeFile_e_Logger );
				logger.addHandler( fh );
				
			} catch (SecurityException | IOException e) { e.printStackTrace();}
    	}
    }

    private void setLogOnOff(){
        if( cfgSensore.log.equalsIgnoreCase( CfgSensore.PAR_STATO_PASSIVO ) ) { disattivaLog(); }
        else { attivaLog();}
    }
    private void setLogOnOff(@NotNull Messaggio msg) {
        if( msg.cfgSensore.log.equalsIgnoreCase( CfgSensore.PAR_STATO_PASSIVO ) ) {
            disattivaLog();
        } else {
            attivaLog();

        }

        try {
            richiestaStatoSensore();
        } catch (IOException e) {
            logger.log(Level.SEVERE , null , e );
        }
    }
    public void disattivaLog() 	{
        logger.setLevel(Level.OFF);
        cfgSensore.log =  CfgSensore.PAR_STATO_PASSIVO;
    }
    public void attivaLog() {
        logger.setLevel(Level.ALL);
        cfgSensore.log =  CfgSensore.PAR_STATO_ATTIVO;
    }

    private void logDaFile( Messaggio tmp ) {
        try {
            tmp.cfgSensore.log = OperazioniSuFile.leggiLog( sensore.getNomeSensore() , Costanti.DEF_RIGHE_LOG );
            spedisciLog( tmp );
        } catch (IOException e) {
            logger.log(Level.SEVERE , null , e );
        }
    }
}
