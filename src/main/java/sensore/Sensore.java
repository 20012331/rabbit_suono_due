/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensore;

import java.awt.geom.Point2D;

import contenitori.EqRetta;
import contenitori.CfgSensore;
import contenitori.Suono;
import partenza.Costanti;

/**
 *
 * @author universita
 */
public class Sensore {

    private static final double VS = 340;

    private Point2D posizioneMappa = new Point2D.Double();
    private Point2D posizioneMicDX = new Point2D.Double();
    private Point2D posizioneMicSX = new Point2D.Double();
    private Point2D posizioneMicUP = new Point2D.Double();
    //private final double DISTANZA_MIC_IN_CM = 0.1;
    
    private double distanzaMicFraLoro;
    private double differenzaArrivoSuono;
    private double angoloArrivo;


    //private static final double ANG_RAD_FULL = 2 * Math.PI;
    
    private String nomeSensore = "";
    private boolean isAttivo = true;

    /**
     * serve solo per il test
     * 
     * ipotizzo un orientamento fisso verso nord.<b>Valutare la possibilita di aggiungere un angolo di orientamento</b>
     * @param x
     * @param y
     * @param distanzaMicFraLoroInMetri
     */
    @Deprecated public Sensore( double x , double y , double distanzaMicFraLoroInMetri ) {    	
    	this.distanzaMicFraLoro = distanzaMicFraLoroInMetri;        
    	setPosizioneMappa( x , y , false);    
    }

    
    /**
     * la distanza fra microfoni e pre impostata
     * @param x longitudine 8.7777...
     * @param y latitudine	45.7777...
     * @param nomeSensore
     */
    public Sensore( double x , double y , String nomeSensore) {
    	this.nomeSensore = nomeSensore;
    	//this.distanzaMicFraLoro = Costanti.GEO_5_CM * 2;
    	// distanza su mappa
    	//this.distanzaMicFraLoro = 0.09852609830207464;
        setPosizioneMappa( x , y , true);
    }

    public Sensore(CfgSensore cfgSensore) {
        this(cfgSensore.lng , cfgSensore.lat, cfgSensore.nSensore );
        setStatoSensore( cfgSensore.stato );
    }

    /**
     * imposto la posizione su mappa e i microfoni a distanza fissa
     * @param x longitudine
     * @param y latitudine
     */
    public void setPosizioneMappa( double x , double y , boolean isSuMappa ) {
        posizioneMappa.setLocation(x,y);
        setDistanzaMic(isSuMappa);
        /*
        posizioneMicDX.setLocation( posizioneMappa.getX() + (distanzaMicFraLoro/2), posizioneMappa.getY() );
        posizioneMicSX.setLocation( posizioneMappa.getX() - ( distanzaMicFraLoro/2 ), posizioneMappa.getY() );
        posizioneMicUP.setLocation( posizioneMappa.getX() , getPosizioneMappa().getY() - ( distanzaMicFraLoro/2 ) );
        */
        
    }

    private void setDistanzaMic( boolean isSuMappa) {
    
        double impDist = distanzaMicFraLoro;
    	if( isSuMappa ) { 
            impDist = Costanti.GEO_5_CM;
            this.distanzaMicFraLoro = daGeo2Metri(  
                posizioneMappa.getY(), posizioneMappa.getX() - impDist , 
                posizioneMappa.getY(), posizioneMappa.getX() + impDist 
            );
        }
    	
    	posizioneMicDX.setLocation( posizioneMappa.getX() + impDist , posizioneMappa.getY() );
        posizioneMicSX.setLocation( posizioneMappa.getX() - impDist , posizioneMappa.getY() );
        posizioneMicUP.setLocation( posizioneMappa.getX() , getPosizioneMappa().getY() + impDist );
    }
    
    public Point2D getPosizioneMappa() {	return posizioneMappa; 	}
    public Point2D getPosizioneMicDX() {	return posizioneMicDX; 	}
    public Point2D getPosizioneMicSX() {	return posizioneMicSX; 	}
    public Point2D getPosizioneMicUP() {	return posizioneMicUP; 	}
    
    public String getNomeSensore() { return nomeSensore; }
    public void setNomeSensore(String nomeSensore ){ 
        this.nomeSensore = nomeSensore;
    }
    
    public boolean isAttivo() { return isAttivo; }
    public void setAttivo(boolean isAttivo) {
        this.isAttivo = isAttivo;

    }
    public void setStatoSensore( String paramConst ) {
        if( paramConst.equalsIgnoreCase( CfgSensore.PAR_STATO_ATTIVO ) ) { setAttivo(true); }
        else if( paramConst.equalsIgnoreCase( CfgSensore.PAR_STATO_PASSIVO ) ){ setAttivo( false ); }
    }
    public String getStatoSensore() {
        if( isAttivo() ) { return CfgSensore.PAR_STATO_ATTIVO; }
        else { return CfgSensore.PAR_STATO_PASSIVO; }
    }
    
    
    public double getAngoloArrivo(Suono suono){
        // angolo = arc_cos ( VS * ritardo )/ distanza microofoni
        double sopra = VS * differenzaArrivoSuono;        
        //double sotto = sopra / (distanzaMicFraLoro / Costanti.GEO_1_CM) ;
        double sotto = sopra / distanzaMicFraLoro;
        //double sotto = sopra / DISTANZA_MIC_IN_CM;
        
        boolean isSotto = 0 < (suono.tempoMicUP - ( (suono.tempoMicSX + suono.tempoMicDX ) / 2 ) );
        
        angoloArrivo = Math.acos( sotto );
        //System.out.println( sopra + ","+ sotto + "," + angoloArrivo);
        
        if( isSotto ) { angoloArrivo = -angoloArrivo;}
        
        return angoloArrivo;
    }
    
    //public double getAngoloArrivo( double tempoMicN , double tempoMicS ){        setTempoArrivo( tempoMicN , tempoMicS );        return getAngoloArrivo();    }

    public void setTempoArrivo( Suono suono){
    //differenzaArrivoSuono = Math.abs( suono.tempoMicDX - suono.tempoMicSX );
            differenzaArrivoSuono = suono.tempoMicSX - suono.tempoMicDX ;
    }

        


    public EqRetta ricevoSuono( Suono suono ) {
        EqRetta retta = new EqRetta();
        setTempoArrivo( suono );
        getAngoloArrivo( suono);

        retta.rad = angoloArrivo;

        // pendenza 
        retta.m = Math.tan( angoloArrivo );
        // se maggiore del minore , allora sta sotto
        //int bias = Double.compare( Double.min(suono.tempoMicSX, suono.tempoMicSX) , suono.tempoMicUP ); 
        //if ( bias > 0 ) { angoloArrivo = -1 * angoloArrivo; }


        // scostamento sull'asse
        retta.q = -1 * ( posizioneMappa.getX() * retta.m - posizioneMappa.getY() ) ;

        retta.lng = getPosizioneMappa().getX();
        retta.lat = getPosizioneMappa().getY();
        retta.nSensore = nomeSensore;
        retta.tipoSuono = suono.tipoSuono;
        retta.id = suono.milli;

        return retta;
    }
	

    /**
     * <b>funzione presa da internet, non perfetta</b><br>
     * misura la distanza in metri fra due punti sulla terra date lat e long 
     * @param lat1 y1
     * @param lon1 x1
     * @param lat2 y1
     * @param lon2 x2
     * @return misura in metri
     */
    public static final double daGeo2Metri( double lat1, double lon1, double lat2, double lon2){  // generally used geo measurement function
        double R = 6378.137; // Radius of earth in KM
        double dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
        double dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
            Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c;
        return d * 1000; // c meters
    }

    
    public CfgSensore getCfgSensore() {
    	CfgSensore cfg 	= new CfgSensore();
        cfg.nSensore 			= getNomeSensore();
        cfg.stato 				= getStatoSensore();
        cfg.lng 				= getPosizioneMappa().getX();
        cfg.lat 				= getPosizioneMappa().getY();
        return cfg;
    }
    
        @Override
    public String toString() {
        return "CoppiaMicrofoni{" +
            "posizioneMappa=" + posizioneMappa +
            ", posizioneMicDX=" + posizioneMicDX +
            ", posizioneMicSX=" + posizioneMicSX +
            ", posizioneMicUP=" + posizioneMicUP +
            ", distanzaMicFraLoro=" + distanzaMicFraLoro +
            ", differenzaArrivoSuono=" + differenzaArrivoSuono +
            ", angoloArrivo=" + angoloArrivo +
            '}';
    }

    public void printDataDebug(){
    	
    	// sistemare angolo e microfoni
    	// versione di primo sviluppo si idea intellij
    	// y = mx + p
    	// -q = mx/y * -1
    	double q = -1 * ( posizioneMappa.getX() * Math.tan( angoloArrivo ) - posizioneMappa.getY() ) ;
    	
        System.out.println( "+------------------------------------+" );
        System.out.println( "| posizioneMappa = [ " + posizioneMappa.getX() + " , " + posizioneMappa.getY() + " ]");
        System.out.println( "+------------------------------------+" );
        System.out.println( "angolo di arrivo : " );
        System.out.println( "[RAD] : " + angoloArrivo);
        System.out.println( "[DEG] : " + (180 * angoloArrivo / Math.PI) );
        System.out.println( "[PENDENZA = m] : " +  Math.tan( angoloArrivo ) );
        System.out.println( "[scostamento = q ] : " + q );
    }
        
}
