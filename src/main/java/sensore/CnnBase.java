package sensore;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.time.Instant;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.rabbitmq.client.BlockedListener;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import contenitori.CfgConnessione;
import optSuFile.OperazioniSuFile;

/**
 * serve come costruttore di base e basta
 */
public class CnnBase {

    /**
     * massimi tentativi di riconnessione
     */
    private static final int MAX_TENTATIVI_RICONNESSIONE = 5;
    private Connection cnn = null;
    private CfgConnessione cfgOpt = null;


    /**
     * inizializza la connessione con dei valori presi dal file ,
     * in caso di errori usa i valori base
     */
    public CnnBase() { this( OperazioniSuFile.leggiFileConnessione() ); }
    public CnnBase( CfgConnessione cfgOpt ) { this.cfgOpt = cfgOpt; }

    /**
     * @see CnnBase#getConnessione(int)
     */
    public Connection getConnessione(){ return getConnessione( MAX_TENTATIVI_RICONNESSIONE ); }
    /**
     * prova ad aprire una connessione e ritenta pe una serie di volte.
     *
     * @param tentativi sovrascrive il numero di tentativi
     * @return una connessione o null se non e' possibile connettersi
     */
    public Connection getConnessione(int tentativi) {

        // eseguo qualche tentativo di riconnesione in caso il server fose down
        // se succede in fase operativa le gestisco in altro modo
        //System.out.println("tentativo connessione verso rabbit ");

        Logger.getLogger( CnnBase.class.getName() ).log(Level.INFO , "tentativo connessione verso rabbit ");
        do{

            try {

                System.out.println( "tentativi rimasti : " + tentativi );
                Logger.getLogger( CnnBase.class.getName() ).log(Level.INFO , "tentativi rimasti");

                cnn = paramBase();
                cnn.addBlockedListener( cnnLockListener );

            } catch (IOException ex) {

                Logger.getLogger( CnnBase.class.getName() ).log( Level.SEVERE, null, ex );
                System.out.println( "ioexcept : " );
                //pausa();

            } catch (TimeoutException ex) {
                System.out.println( "timeout" );
                Logger.getLogger( CnnBase.class.getName() ).log( Level.SEVERE, null, ex );
                //pausa();

            }

            tentativi = tentativi - 1;

        }while( ( cnn == null ) && ( tentativi > 0 ) );

        return cnn;
    }
	
        	
    private Connection paramBase() throws IOException, TimeoutException{
    // impostare le connessioni
        ConnectionFactory cFact = new ConnectionFactory();
        cFact.setUsername(cfgOpt.USER_NAME);
        cFact.setPassword(cfgOpt.PASSWORD);
        cFact.setVirtualHost(cfgOpt.VIRTUAL_HOST);
        cFact.setHost(cfgOpt.HOST_NAME);
        cFact.setPort(cfgOpt.NUMERO_PORTA -1 );

        // serve ad una connessione sicura con un certificato insicuro
        try { cFact.useSslProtocol(); }
        catch (KeyManagementException | NoSuchAlgorithmException e) { e.printStackTrace();        }

        cFact.setAutomaticRecoveryEnabled( true );
        cFact.setNetworkRecoveryInterval( 5000 );

        return cFact.newConnection();
    }



    // forse viene lanciato quando la connessione si interrompe
    private BlockedListener cnnLockListener = new BlockedListener() {

        @Override
        public void handleUnblocked() throws IOException {
            Logger.getLogger( CnnBase.class.getName() )
                .log(
                    Level.INFO , 
                    "Connessio al server ripristinata : " +
                        Date.from( Instant.now() ).toString() 
                );

        }

        @Override
        public void handleBlocked(String reason) throws IOException {
            Logger.getLogger( CnnBase.class.getName() )
                .log(
                    Level.INFO , 
                    "Connessio al server in attesa di ripristino : " + 
                        Date.from( Instant.now() ).toString() 
                );

        }
    };
	
}
